/*
 Navicat Premium Data Transfer

 Source Server         : Localhost_MariaDB_10.4
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : proseller

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 31/03/2020 15:49:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for banks
-- ----------------------------
DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(11) NULL DEFAULT NULL,
  `bankcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bankname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shortname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

DELETE FROM `banks`;
INSERT INTO `banks` (`id`, `bankcode`, `bankname`, `shortname`) VALUES
  (1, 'mbb', 'Maybank', 'Maybank'),
  (2, 'cimb', 'CIMB Bank', 'CIMB'),
  (3, 'pb', 'Public Bank', 'Public'),
  (4, 'rhb', 'RHB Bank', 'RHB'),
  (5, 'amb', 'AmBank', 'Ambank'),
  (6, 'bim', 'Bank Islam', 'Bank Islam'),
  (7, 'bsn', 'Bank Simpanan Nasional', 'BSN'),
  (8, 'citi', 'Citibank', 'Citibank'),
  (9, 'affin', 'Affin Bank', 'Affin'),
  (10, 'alliance', 'Alliance Bank', 'Alliance'),
  (11, 'hlb', 'Hong Leong Bank', 'Hong Leong'),
  (12, 'hsbc', 'HSBC Bank', 'HSBC'),
  (13, 'ocbc', 'OCBC Bank', 'OCBC'),
  (14, 'br', 'Bank Rakyat', 'Bank Rakyat');

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `id` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `levelName` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
DELETE FROM `level`;
INSERT INTO `level` (`id`, `levelName`) VALUES
  ('1', 'Administrator'),
  ('2', 'staff-01'),
  ('adm001', 'SUPERADMIN');
-- ----------------------------
-- Table structure for logstok
-- ----------------------------
DROP TABLE IF EXISTS `logstok`;
CREATE TABLE `logstok`  (
  `id` int(11) NULL DEFAULT NULL,
  `last` int(11) NULL DEFAULT NULL,
  `new` int(11) NULL DEFAULT NULL,
  `type` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `produk` int(11) NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `time` int(11) NULL DEFAULT NULL,
  `referrer` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NULL DEFAULT NULL,
  `name` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `add1` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `add2` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postcode` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `states_id` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `info` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unit` int(11) NULL DEFAULT NULL,
  `product` int(11) NULL DEFAULT NULL,
  `price` double NULL DEFAULT NULL,
  `bank` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bankdate` date NULL DEFAULT NULL,
  `banktime` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pdflink` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resit` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postage` int(11) NULL DEFAULT NULL,
  `postmethod` int(11) NULL DEFAULT NULL,
  `trackingno` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `trackingdate` date NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `iscancel` int(11) NULL DEFAULT NULL,
  `referrer` int(11) NULL DEFAULT NULL,
  `print` int(11) NULL DEFAULT NULL,
  `ip` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `update_time` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` date NULL DEFAULT NULL,
  `update_user` int(11) NULL DEFAULT NULL,
  `sellerid` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for postage
-- ----------------------------
DROP TABLE IF EXISTS `postage`;
CREATE TABLE `postage`  (
  `id` int(11) NULL DEFAULT NULL,
  `area` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` double NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
DELETE FROM `postage`;
INSERT INTO `postage` (`id`, `area`, `price`) VALUES
  (1, 'sm', 7),
  (2, 'ss', 10),
  (3, 'cod', 0);

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk`  (
  `id` int(11) NULL DEFAULT NULL,
  `produk` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `unit` int(11) NULL DEFAULT NULL,
  `hargakos` double NULL DEFAULT NULL,
  `user` int(11) NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `time` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for states
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states`  (
  `id` int(11) NULL DEFAULT NULL,
  `name` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name_long` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state_id` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state_code` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
DELETE FROM `states`;
INSERT INTO `states` (`id`, `name`, `name_long`, `state_id`, `state_code`) VALUES
  (1, 'JOHOR', 'JOHOR DARUL TAKZIM', '01', 'JH'),
  (2, 'KEDAH', 'KEDAH DARUL AMAN', '02', 'KD'),
  (3, 'KELANTAN', 'KELANTAN DARUL NAIM', '03', 'KT'),
  (4, 'MELAKA', 'MELAKA BANDARAYA BERSEJARAH', '04', 'ML'),
  (5, 'NEGERI SEMBILAN', 'NEGERI SEMBILAN DARUL KHUSUS', '05', 'NS'),
  (6, 'PAHANG', 'PAHANG DARUL MAKMUR', '06', 'PH'),
  (7, 'PULAU PINANG', 'PULAU PINANG PULAU MUTIARA', '07', 'PN'),
  (8, 'PERAK', 'PERAK DARUL RIDZUAN', '08', 'PR'),
  (9, 'PERLIS', 'PERLIS INDERA KAYANGAN', '09', 'PL'),
  (10, 'SELANGOR', 'SELANGOR DARUL EHSAN', '10', 'SG'),
  (11, 'TERENGGANU', 'TERENGGANU DARUL IMAN', '11', 'TR'),
  (12, 'SABAH', 'SABAH NEGERI DI BAWAH BAYU', '12', 'SB'),
  (13, 'SARAWAK', 'SARAWAK BUMI KENYALANG', '13', 'SR'),
  (14, 'WP KUALA LUMPUR', 'WILAYAH PERSEKUTUAN KUALA LUMPUR', '14', 'KL'),
  (15, 'WP LABUAN', 'WILAYAH PERSEKUTUAN LABUAN', '15', 'LB'),
  (16, 'WP PUTRAJAYA', 'WILAYAH PERSEKUTUAN PUTRAJAYA', '16', 'PJ');
  
-- ----------------------------
-- Table structure for tokens
-- ----------------------------
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens`  (
  `token` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `date_create` date NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NULL DEFAULT NULL,
  `name` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `poskod` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `town` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `lastlogin` int(11) NULL DEFAULT NULL,
  `ip` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `referrer` int(11) NULL DEFAULT NULL,
  `codepos` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `reset` int(11) NULL DEFAULT NULL,
  `company` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gst` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gstrate` int(11) NULL DEFAULT NULL,
  `picture` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `smsname` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `smspass` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `smson` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
DELETE FROM `users`;
INSERT INTO `users` VALUES (1, 'MY Store', 'superadmin', 'BwulEGFVTB.dc80b52e67ed90a65fdbdddd8035f41a', '', '', '', '', '', '0123456789', 'adm001', 1, 1585640410, '127.0.0.1', 1, NULL, NULL, 'my-12344-xx', 'sdd3434', NULL, 'superadmin_20180714-1176140151.png', 'testing', 'testing', NULL);
SET FOREIGN_KEY_CHECKS = 1;
