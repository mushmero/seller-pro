/*set timeout hide*/
setTimeout(function(){
    $("#alert").hide();
}, 2000);
/*tracking.my api*/
function linkTrack(num) {
  TrackButton.track({
    tracking_no: num
  });
}
$('#dp1').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('#dps1').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('#dp2').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('#dps2').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('#dp3').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('#dps3').datepicker({
    format: "M/dd/yyyy",
    todayBtn: "linked",
    todayHighlight: true,
    weekStart: 1
});
$('.clockpicker').clockpicker({
  twelvehour: true
});
$(document).ajaxStart(function() { Pace.restart(); });
  //Default colours
  $.brandPrimary =  '#20a8d8';
  $.brandSuccess =  '#4dbd74';
  $.brandInfo =     '#63c2de';
  $.brandWarning =  '#f8cb00';
  $.brandDanger =   '#f86c6b';

  $.grayDark =      '#2a2c36';
  $.gray =          '#55595c';
  $.grayLight =     '#818a91';
  $.grayLighter =   '#d1d4d7';
  $.grayLightest =  '#f8f9fa';
$(document).ready(function(){
    sendData(this);
});
function sendData(o){
    var val = o.value;
        if(typeof val == 'undefined' || val == 'all'){
            $.ajax({
                type: "POST",
                url: "dashboard/getAllMonth?month=all",
                data:{
                    "price":"price",
                    "date": "date",
                },
                success: function(resp){  
                    // Prepare Json data Start
                    var obj = jQuery.parseJSON(resp);
                    setAll(obj);
                },
                error:function(event, textStatus, errorThrown) {
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            }); 
        }else if(val != null || val != 'all' || typeof val != 'undefined'){
            $.ajax({
                type: "POST",
                url: "dashboard/getSelectedMonth?month="+val,
                data:{
                    "price":"price",
                    "date": "date",
                },
                success: function(resp){  
                    // Prepare Json data Start
                    var obj = jQuery.parseJSON(resp);
                    setSelected(obj);
                },
                error:function(event, textStatus, errorThrown) {
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }
}

function setAll(obj){
    var Data = [];
    var Labels = [];
    $.each(obj, function(key, data){
        Data.push(data.price);
        Labels.push(data.date);
    });
    var lineData = {
        labels : Labels,
        datasets: [{
            label: "Monthly Sales",
            data:Data,
            backgroundColor: $.brandInfo,
            borderColor: $.brandInfo,
            pointBackgroundColor: $.brandInfo,
            borderWidth: 1
        }],
    };
    var lineOptions = {
        scales: {
            yAxes:[{
                ticks:{
                    beginAtZero:true,
                }
            }]
        }
    };
    var config = {
        type:'line',
        data:lineData,
        options:lineOptions
    };
    var cvs = $('#lineChart').get(0).getContext('2d');
    if(typeof lineChart != 'undefined' ){
        lineChart.destroy();
    }    
    var lineChart = new Chart(cvs, config);
}
function setSelected(obj){
    var Data = [];
    var Labels = [];
    $.each(obj, function(key, data){
        Data.push(data.price);
        Labels.push(data.date);
    });
    var lineData = {
        labels : Labels,
        datasets: [{
            label: "Monthly Sales",
            data:Data,
            backgroundColor: $.brandSuccess,
            borderColor: $.brandSuccess,
            pointBackgroundColor: $.brandSuccess,
            borderWidth: 1
        }],
    };
    var lineOptions = {
        scales: {
            yAxes:[{
                ticks:{
                    beginAtZero:true,
                }
            }]
        }
    };
    var config = {
        type:'line',
        data:lineData,
        options:lineOptions
    };
    var cvs = $('#lineChart').get(0).getContext('2d');
    if(typeof lineChart != 'undefined' ){
        lineChart.destroy();
    }    
    var lineChart = new Chart(cvs, config);
}

/*pie chart*/
$(document).ready(function () {
    var pieData = {
        datasets:[{
            data: [5,10,15,20],
            backgroundColor:[
                    'rgba(255, 99, 132, 0.4)',
                    'rgba(54, 162, 235, 0.4)',
                    'rgba(255, 206, 86, 0.4)',
                    'rgba(75, 192, 192, 0.4)'
                ],
                borderColor:[
                    'rgba(0,0,0,0.6)',
                    'rgba(0,0,0,0.6)',
                    'rgba(0,0,0,0.6)',
                    'rgba(0,0,0,0.6)'
                ],
                borderWidth:1
        }],
    };
    var pieOption = {
        cutoutPercentage:50,
        rotation:0.5 * Math.PI,
        circumference:2 * Math.PI,
        animation:{
            animateRotate:true
        }
    };
    var cvs = $('#pieChart').get(0).getContext('2d');
    if(typeof pieChart != 'undefined' ){
        pieChart.destroy();
    }
    var pieChart = new Chart(cvs, {
        type:'doughnut',
        data:pieData,
        options:pieOption
    });
});
/* end of pie chart */