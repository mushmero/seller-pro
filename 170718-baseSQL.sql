-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.13-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5280
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table sistem_jualan.banks
CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bankcode` varchar(50) CHARACTER SET latin1 NOT NULL,
  `bankname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `shortname` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table sistem_jualan.banks: ~14 rows (approximately)
DELETE FROM `banks`;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` (`id`, `bankcode`, `bankname`, `shortname`) VALUES
	(1, 'mbb', 'Maybank', 'Maybank'),
	(2, 'cimb', 'CIMB Bank', 'CIMB'),
	(3, 'pb', 'Public Bank', 'Public'),
	(4, 'rhb', 'RHB Bank', 'RHB'),
	(5, 'amb', 'AmBank', 'Ambank'),
	(6, 'bim', 'Bank Islam', 'Bank Islam'),
	(7, 'bsn', 'Bank Simpanan Nasional', 'BSN'),
	(8, 'citi', 'Citibank', 'Citibank'),
	(9, 'affin', 'Affin Bank', 'Affin'),
	(10, 'alliance', 'Alliance Bank', 'Alliance'),
	(11, 'hlb', 'Hong Leong Bank', 'Hong Leong'),
	(12, 'hsbc', 'HSBC Bank', 'HSBC'),
	(13, 'ocbc', 'OCBC Bank', 'OCBC'),
	(14, 'br', 'Bank Rakyat', 'Bank Rakyat');
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;

-- Dumping structure for table sistem_jualan.level
CREATE TABLE IF NOT EXISTS `level` (
  `id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `levelName` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sistem_jualan.level: ~2 rows (approximately)
DELETE FROM `level`;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` (`id`, `levelName`) VALUES
	('1', 'Administrator'),
	('2', 'staff-01'),
	('adm001', 'SUPERADMIN');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;

-- Dumping structure for table sistem_jualan.logstok
CREATE TABLE IF NOT EXISTS `logstok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last` int(11) NOT NULL,
  `new` int(11) NOT NULL,
  `type` varchar(60) CHARACTER SET latin1 NOT NULL,
  `produk` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` int(11) NOT NULL,
  `referrer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping structure for table sistem_jualan.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET latin1 NOT NULL,
  `add1` varchar(255) CHARACTER SET latin1 NOT NULL,
  `add2` varchar(255) CHARACTER SET latin1 NOT NULL,
  `postcode` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city` varchar(255) CHARACTER SET latin1 NOT NULL,
  `states_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(60) CHARACTER SET latin1 NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 NOT NULL,
  `info` varchar(255) CHARACTER SET latin1 NOT NULL,
  `unit` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `price` float NOT NULL,
  `bank` varchar(60) CHARACTER SET latin1 NOT NULL,
  `bankdate` date NOT NULL,
  `banktime` varchar(60) CHARACTER SET latin1 NOT NULL,
  `pdflink` varchar(60) CHARACTER SET latin1 NOT NULL,
  `resit` varchar(60) CHARACTER SET latin1 NOT NULL,
  `postage` int(11) NOT NULL DEFAULT 0,
  `postmethod` int(11) NOT NULL,
  `trackingno` varchar(60) CHARACTER SET latin1 NOT NULL,
  `trackingdate` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `iscancel` int(11) NOT NULL DEFAULT 0,
  `referrer` int(11) NOT NULL,
  `print` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(60) CHARACTER SET latin1 NOT NULL,
  `time` varchar(50) CHARACTER SET latin1 NOT NULL,
  `date` date NOT NULL,
  `update_time` varchar(50) CHARACTER SET latin1 NOT NULL,
  `update_date` date NOT NULL,
  `update_user` int(11) NOT NULL,
  `sellerid` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping structure for table sistem_jualan.postage
CREATE TABLE IF NOT EXISTS `postage` (
  `id` int(11) NOT NULL,
  `area` varchar(50) CHARACTER SET latin1 NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sistem_jualan.postage: ~2 rows (approximately)
DELETE FROM `postage`;
/*!40000 ALTER TABLE `postage` DISABLE KEYS */;
INSERT INTO `postage` (`id`, `area`, `price`) VALUES
	(1, 'sm', 7),
	(2, 'ss', 10),
	(3, 'cod', 0);
/*!40000 ALTER TABLE `postage` ENABLE KEYS */;

-- Dumping structure for table sistem_jualan.produk
CREATE TABLE IF NOT EXISTS `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produk` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `unit` int(11) NOT NULL DEFAULT 0,
  `hargakos` float NOT NULL,
  `user` int(11) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Dumping structure for table sistem_jualan.states
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `name_long` varchar(50) CHARACTER SET latin1 NOT NULL,
  `state_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `state_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Dumping data for table sistem_jualan.states: ~16 rows (approximately)
DELETE FROM `states`;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` (`id`, `name`, `name_long`, `state_id`, `state_code`) VALUES
	(1, 'JOHOR', 'JOHOR DARUL TAKZIM', '01', 'JH'),
	(2, 'KEDAH', 'KEDAH DARUL AMAN', '02', 'KD'),
	(3, 'KELANTAN', 'KELANTAN DARUL NAIM', '03', 'KT'),
	(4, 'MELAKA', 'MELAKA BANDARAYA BERSEJARAH', '04', 'ML'),
	(5, 'NEGERI SEMBILAN', 'NEGERI SEMBILAN DARUL KHUSUS', '05', 'NS'),
	(6, 'PAHANG', 'PAHANG DARUL MAKMUR', '06', 'PH'),
	(7, 'PULAU PINANG', 'PULAU PINANG PULAU MUTIARA', '07', 'PN'),
	(8, 'PERAK', 'PERAK DARUL RIDZUAN', '08', 'PR'),
	(9, 'PERLIS', 'PERLIS INDERA KAYANGAN', '09', 'PL'),
	(10, 'SELANGOR', 'SELANGOR DARUL EHSAN', '10', 'SG'),
	(11, 'TERENGGANU', 'TERENGGANU DARUL IMAN', '11', 'TR'),
	(12, 'SABAH', 'SABAH NEGERI DI BAWAH BAYU', '12', 'SB'),
	(13, 'SARAWAK', 'SARAWAK BUMI KENYALANG', '13', 'SR'),
	(14, 'WP KUALA LUMPUR', 'WILAYAH PERSEKUTUAN KUALA LUMPUR', '14', 'KL'),
	(15, 'WP LABUAN', 'WILAYAH PERSEKUTUAN LABUAN', '15', 'LB'),
	(16, 'WP PUTRAJAYA', 'WILAYAH PERSEKUTUAN PUTRAJAYA', '16', 'PJ');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;

-- Dumping structure for table sistem_jualan.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `token` varchar(50) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping structure for table sistem_jualan.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET latin1 NOT NULL,
  `username` varchar(60) CHARACTER SET latin1 NOT NULL,
  `password` varchar(60) CHARACTER SET latin1 NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 NOT NULL,
  `address` varchar(60) CHARACTER SET latin1 NOT NULL,
  `poskod` varchar(60) CHARACTER SET latin1 NOT NULL,
  `town` varchar(60) CHARACTER SET latin1 NOT NULL,
  `state` varchar(60) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(60) CHARACTER SET latin1 NOT NULL,
  `level` varchar(50) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `lastlogin` int(11) NOT NULL,
  `ip` varchar(60) CHARACTER SET latin1 NOT NULL,
  `referrer` int(11) NOT NULL DEFAULT 0,
  `codepos` varchar(60) CHARACTER SET latin1 NOT NULL,
  `reset` int(11) NOT NULL,
  `company` varchar(100) CHARACTER SET latin1 NOT NULL,
  `gst` varchar(60) CHARACTER SET latin1 NOT NULL,
  `gstrate` int(11) NOT NULL DEFAULT 6,
  `picture` varchar(60) CHARACTER SET latin1 NOT NULL DEFAULT '/img/no-logo.png',
  `smsname` varchar(60) CHARACTER SET latin1 NOT NULL,
  `smspass` varchar(60) CHARACTER SET latin1 NOT NULL,
  `smson` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table sistem_jualan.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `address`, `poskod`, `town`, `state`, `phone`, `level`, `status`, `lastlogin`, `ip`, `referrer`, `codepos`, `reset`, `company`, `gst`, `gstrate`, `picture`, `smsname`, `smspass`, `smson`) VALUES
	(1, 'MY Store', 'superadmin', '03GNv4OnXx.47f95e3acfecfd3864ed1b53fe68d32e', 'admin@osp.com', 'Plaza Hentian Kajang', '41200', 'Kajang', '10', '0123456789', 'adm001', 1, 1531817170, '127.0.0.1', 1, '', 0, 'my-12344-xx', 'sdd3434', 0, 'superadmin_20180714-1176140151.png', 'testing', 'testing', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
