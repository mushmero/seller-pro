<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	var $data = array();

	function __construct() {
		parent::__construct();

		$this->load->model('musers');
	}

	public function index() {
		if ($this->musers->isLoggedIn()) {
			redirect('dashboard');
		}
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($user = $this->musers->checkUser($username)) {
				if ($this->musers->verifyPassword($password, $user['password'])) {
					$this->musers->allow_pass($user);
					$this->musers->setLoginData($user);
					redirect('dashboard');
				} else {
					$this->data['login_error'] = 'Invalid username or password';
				}
			} else {
				$this->data['login_error'] = 'Username not found';
			}
		}
		$this->data['user'] = $this->session->userdata('user');
		$this->load->view('login/vlogin', $this->data);
	}

	public function logout() {
		$this->musers->remove_pass();
		$this->data['login_success'] = 'You have been logged out. Thank you.';
		$this->load->view('login/vlogin', $this->data);
	}

	public function noaccess() {
		$this->data['login_error'] = 'You do not have access or your login has expired.';
		$this->load->view('login/vlogin', $this->data);
	}

	public function forgot() {
		if ($this->musers->isLoggedIn()) {
			$user_data = $this->session->userdata('user');
			redirect('dashboard');
		}

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ($this->form_validation->run()) {
			$email = $this->input->post('email');
			$validEmail = $this->musers->get_user_by_email($email);
			if (!$validEmail) {
				$this->data['email_error'] = 'Email not found';
			}

			$token = $this->musers->insertToken($validEmail);
			$encodeToken = $this->base64url_encode($token);
			$url = site_url() . 'reset/token/' . $encodeToken;

			if (ENVIRONMENT == 'development') {
				$this->data['reset_button'] = 'Click here to <a href="' . $url . '">reset</a>';
			} else {
				require_once "vendor/autoload.php";
				$data = array(
					'host' => mailhost,
					'port' => mailport,
					'username' => mailuser,
					'password' => mailpass,
					'secure' => 'tls',
					'from' => 'no-reply@' . $_SERVER['HTTP_HOST'],
					'name' => 'Administrator@' . $_SERVER['HTTP_HOST'],
					'to' => $email,
					'subject' => 'Password Reset for ' . $email,
					'content' => 'You have requested to reset your password. If you perform this request, please click <a href="' . $url . '"> LINK</a> to reset your password. If you did not perform this request, please contact webmaster.',
				);

				if ($this->mailer->reset_pass_mailer($data)) {
					$this->data['reset_button'] = "Message has been sent successfully. Please check your email.";
				} else {
					$this->data['reset_button'] = "Unable to send email.";
				}
			}
		}

		$this->load->view('login/vforgot', $this->data);
	}

	public function reset_password() {
		$token = $this->base64url_decode($this->uri->segment(3));
		$info = $this->musers->checkToken($token);
		if (!$info) {
			$this->data['token_error'] = 'Invalid token';
		}
		$this->data = array('token' => $this->base64url_encode($token));
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');
		if ($this->form_validation->run()) {
			$newUpdate['password'] = $this->musers->saltedHash($this->input->post('password'));
			$newUpdate['id'] = $info->id;
			if ($this->musers->updatePassword($newUpdate)) {
				$this->data['update_success'] = 'Password is successfully reset. <a href="' . site_url() . '">Click here</a> to login';
			} else {
				$this->data['update_error'] = 'Error resetting password. Please contact ' . $emailAdd;
			}
		}

		$this->load->view('login/vreset', $this->data);
	}

	public function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	public function base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}
