<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	var $data = array();

	function __construct() {
		parent::__construct();
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->model('mdashboard');
		$this->load->model('musers');
		$this->load->library('pdfgenerator');
		$this->load->library('excelgenerator');
		$this->load->library('custompaging');
		$this->load->library('taxation');
		$this->load->library('systools');

		if ($this->musers->isLoggedIn() === FALSE) {
			$this->musers->remove_pass();
			redirect('login/noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}

	public function index() {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserInfo($data['id']);
		/* top widget */
		$this->data['new'] = $this->mdashboard->getNewOrders(date('Y-m-d'));
		$this->data['month'] = $this->mdashboard->getMonthlyOrders(date('m'));
		$this->data['total'] = $this->mdashboard->getTotalOrders();
		$this->data['rejected'] = $this->mdashboard->getRejectOrders();
		/* end of top widget */
		/* graph dropdown list */
		$this->data['monthview'] = $this->mdashboard->getMonth();
		/* end of graph dropdown list */
		/* unit sold */
		$totalUnit = $this->mdashboard->getTotalUnit();
		$soldUnit = $this->mdashboard->getUnitSold();
		$this->data['totalunit'] = $totalUnit[0]->total != '' ? $totalUnit[0]->total : 0;
		$this->data['soldunit'] = $soldUnit[0]->sold != '' ? $soldUnit[0]->sold : 0;
		if ($totalUnit[0]->total == '' || $soldUnit[0]->sold == '') {
			$this->data['upercent'] = 0;
		} else {
			$this->data['upercent'] = intval(($soldUnit[0]->sold / $totalUnit[0]->total) * 100);
		}
		/* end of unit sold */
		/* product availability */
		$totalProduct = $this->mdashboard->countTotalProduct();
		$availProduct = $this->mdashboard->countAvailProduct();
		$this->data['ptotal'] = $totalProduct;
		$this->data['pavail'] = $availProduct;
		if ($totalProduct == 0 || $availProduct == 0) {
			$this->data['ppercent'] = 0;
		} else {
			$this->data['ppercent'] = intval(($availProduct / $totalProduct) * 100);
		}
		/* product availibility */
		/* posted product */
		$accepted = $this->mdashboard->getAcceptedOrders();
		$posted = $this->mdashboard->getPostedOrders();
		$this->data['accepted'] = $accepted;
		$this->data['posted'] = $posted;
		if ($accepted->accept == 0 || $posted->posted == 0) {
			$this->data['postpercent'] = 0;
		} else {
			$this->data['postpercent'] = intval(($posted->posted / $accepted->accept) * 100);
		}
		/* end of posted product */
		/* expected sales */
		$expected = $this->mdashboard->getExpectedSales();
		$current = $this->mdashboard->getCurrentSales();
		$this->data['expected'] = sprintf('%0.2f', $expected[0]->expected);
		$this->data['current'] = sprintf('%0.2f', $current[0]->current);
		if ($expected[0]->expected == 0 || $current[0]->current == 0) {
			$this->data['exppercent'] = 0;
		} else {
			$this->data['exppercent'] = floatval(($this->data['current'] / $this->data['expected']) * 100);
		}
		/* end of expected sales*/
		/* total revenue */
		$revenue = $this->mdashboard->getTotalSales();
		$this->data['revenue'] = sprintf('%0.2f', $revenue[0]->total);
		/* end of total revenue */
		/* total profit */
		$profit = $this->mdashboard->getNettProfit();
		$this->data['profit'] = sprintf('%0.2f', $profit[0]->totalsales - $profit[0]->postagecost - $profit[0]->productcost);
		/* end of total profit */
		/* total cost */
		$cost = $this->mdashboard->getTotalCost();
		$this->data['cost'] = sprintf('%0.2f', $cost[0]->postagecost + $cost[0]->productcost);
		/* end of total cost*/
		/* efficiency ratio */
		if ($this->data['profit'] == 0 || $this->data['cost'] == 0) {
			$this->data['efficiency'] = 0;
		} else {
			$this->data['efficiency'] = sprintf('%0.2f', ($this->data['cost'] / $this->data['revenue']) * 100);
		}
		/* end of efficiency ratio */
		$this->load->view('dashboard/vdashboard', $this->data);
	}
	public function getAllMonth() {
		if ($_GET['month'] == 'all') {
			echo $this->mdashboard->graphAll();
		}
	}
	public function getSelectedMonth() {
		$month = $_GET['month'];
		echo $this->mdashboard->graphMonth($month);
	}
	public function orders() {

		$this->data['total_items'] = $this->mdashboard->countOrdersAll();
		$this->data['order'] = $this->mdashboard->getOrdersbyTime($this->custompaging->per_page, $this->custompaging->offset);
		$this->data['paging'] = $this->custompaging->get_pagination($this->data['total_items'], $this->custompaging->per_page);

		$postage = $this->input->post('postage');
		$cod = $this->input->post('cod');
		$pid = $this->input->post('pid');
		$states = $this->input->post('states');

		if ($postage == '' && $cod != '') {
			$data = array(
				'postmethod' => $cod,
				'postage' => 3,
			);
			if ($this->mdashboard->updatePostage($data, $pid)) {
				$this->data['success'] = 'Order updated to COD';
			} else {
				$this->data['error'] = 'Unable to process order';
			}
		} else if ($cod == '' && $postage != '') {
			if ($states == '12' || $states == '13' || $states == '15') {
				$cost = 2;
			} else {
				$cost = 1;
			}
			$data = array(
				'postmethod' => $postage,
				'postage' => $cost,
			);
			if ($this->mdashboard->updatePostage($data, $pid)) {
				$this->data['success'] = 'Order updated to track via Postage';
			} else {
				$this->data['error'] = 'Unable to process order';
			}
		}

		$this->load->view('dashboard/vorderlist', $this->data);
	}
	public function viewOrder($id) {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserInfo($data['id']);
		$orders = $this->mdashboard->getOrdersbyId($id);
		$gstprice = $this->taxation->calc_gst($user['gstrate'], (($orders[0]->price * $orders[0]->unit) - $orders[0]->postprice), $orders[0]->postprice);
		if ($orders[0]->postmethod == 2) {
			$this->data['reason'] = 'Buyer request Cash On Delivery';
		} else if ($orders[0]->postmethod == 1) {
			$this->data['reason'] = 'Tracking via postage';
		} else {
			$this->data['reason'] = 'No postage method';
		}
		$this->data['gst'] = $gstprice;
		$this->data['gstrate'] = $user['gstrate'];
		$this->data['orders'] = $orders;
		$this->data['shipping_address'] = $orders[0]->add1 . ' ' . $orders[0]->add2 . ' ' . $orders[0]->postcode . ' ' . $orders[0]->city . ' ' . $orders[0]->states_name;

		$cancel = $this->input->post('cancel');
		$reaccept = $this->input->post('reaccept');
		$complete = $this->input->post('complete');
		$oid = $this->input->post('oid');

		if ($cancel != '' && $reaccept == '') {
			if ($this->mdashboard->updateCanceledOrder($cancel, $oid)) {
				$this->data['success'] = 'Order #' . $oid . ' has been canceled';
				header("Refresh:1,url=" . $oid);
			} else {
				$this->data['error'] = 'Unable to process action';
			}
		} else if ($cancel == '' && $reaccept != '') {
			if ($this->mdashboard->updateCanceledOrder($reaccept, $oid)) {
				$this->data['success'] = 'Order #' . $oid . ' has been re-accepted';
				header("Refresh:1,url=" . $oid);
			} else {
				$this->data['error'] = 'Unable to process action';
			}
		}
		if ($complete != '') {
			if ($this->mdashboard->updateCompleteOrder($complete, $oid)) {
				$this->data['success'] = 'Order #' . $oid . ' has been completed';
				header("Refresh:1,url=" . $oid);
			} else {
				$this->data['error'] = 'Unable to process action';
			}
		}

		$this->load->view('dashboard/vorder', $this->data);
	}
	public function editOrder($id) {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserId($data['id']);
		$this->data['products'] = $this->mdashboard->getProduct($user);
		$this->data['banks'] = $this->mdashboard->getBank();
		$this->data['states'] = $this->mdashboard->getStates();
		$orders = $this->mdashboard->getOrdersbyId($id);
		$this->data['orders'] = $orders;

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('add1', 'Address', 'trim|required');
		$this->form_validation->set_rules('add2', 'Address', 'trim');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required|numeric');
		$this->form_validation->set_rules('cities', 'City', 'trim|required');
		$this->form_validation->set_rules('states', 'State', 'required');
		$this->form_validation->set_rules('product', 'Products', 'required');
		$this->form_validation->set_rules('unit', 'Unit', 'required');
		$this->form_validation->set_rules('bank', 'Bank', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('times', 'Time', 'required');

		if ($this->form_validation->run()) {
			if (!is_dir('uploads/resit/')) {
				mkdir('uploads/resit/', 0777, true);
			}
			$config['upload_path'] = './uploads/resit/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
			$config['file_name'] = date('Ymd') . '-' . mt_rand();
			$config['file_ext_tolower'] = true;
			$config['max_size'] = 5000;
			$config['encrypt_name'] = true;
			$config['remove_spaces'] = true;

			$this->upload->initialize($config);
			$pdf = date('Ymd') . '_' . $this->input->post('phone') . '.pdf';
			$this->upload->data();

			/*$regex1 = '/([0-9].+)\s?/';
				$regex2 = '/([a-zA-Z].+)\s?/';*/
			$str = $this->input->post('states');
			/*preg_match($regex1, $str, $states_id);
				preg_match($regex2, $str, $states_name);*/
			if ($str == '12' || $str == '13' || $str == '15') {
				$postage = 2;
			} else {
				$postage = 1;
			}
			$data = array(
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone'),
				'email' => $this->input->post('email'),
				'add1' => strtoupper($this->input->post('add1')),
				'add2' => strtoupper($this->input->post('add2')),
				'postcode' => strtoupper($this->input->post('postcode')),
				'city' => strtoupper($this->input->post('cities')),
				'states_id' => $str,
				'product' => $this->input->post('product'),
				'unit' => $this->input->post('unit'),
				'bank' => $this->input->post('bank'),
				'bankdate' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'banktime' => $this->input->post('times'),
				'resit' => $this->upload->data('file_name'),
				'pdflink' => $pdf,
				'price' => $this->input->post('price'),
				'referrer' => $user,
				'sellerid' => $data['id'],
				'update_time' => time(),
				'postage' => $postage,
				'update_date' => date('Y-m-d'),
				'ip' => $this->systools->getIP(),
				'info' => $this->input->post('notes'),
			);
			$id = $this->input->post('product');
			$prevUnit = $this->mdashboard->getProductUnit($id);
			$newUnit = $this->input->post('unit');
			$change = (($prevUnit->unit) - $newUnit);
			$changes = array(
				'unit' => $change,
			);
			if ($this->mdashboard->updateStock($changes, $id)) {
				$rslt = array(
					'last' => $prevUnit->unit,
					'new' => $change,
					'type' => 4,
					'produk' => $id,
					'time' => time(),
					'referrer' => $user,
					'date' => date('Y-m-d'),
				);
				$this->mdashboard->insertLogStock($rslt);
			}
			if ($this->mdashboard->updOrder($data, $orders[0]->id)) {
				$this->data['success'] = 'Your order have been updated';
			} else {
				$this->data['error'] = 'Order failed to update. Please try again.';
			}
		}

		$this->load->view('dashboard/veditorder', $this->data);
	}
	public function invoice($id) {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserInfo($data['id']);
		$this->data['user'] = $user;
		$count = $this->mdashboard->countOrdersbyID($id, $user['id']);
		if ($count->result() > 0) {
			if ($this->mdashboard->updateOrderStatus($id)) {
				$order = $this->mdashboard->getOrdersbyId($id);
				$this->data['order'] = $order;
				$this->data['perunit'] = sprintf('%0.2f', (($order[0]->price * $order[0]->unit) - $order[0]->postprice));
				$this->data['subtotal'] = sprintf('%0.2f', ((($order[0]->price * $order[0]->unit) - $order[0]->postprice)));
				$this->data['total'] = sprintf('%0.2f', (0 + $this->data['subtotal']));
				$this->data['shipping'] = sprintf('%0.2f', $order[0]->postprice);
				$this->data['gst'] = $this->taxation->calc_gst((($order[0]->price * $order[0]->unit) - $order[0]->postprice), $order[0]->postprice);
				$this->data['gtotal'] = sprintf('%0.2f', ($this->data['total'] + $this->data['shipping']));
				$this->data['invoice_id'] = 'INV-' . date('Ymd', strtotime($order[0]->date)) . '-' . $order[0]->name;
				$pdflink = $order[0]->pdflink;
				$html = $this->load->view('dashboard/vinvoice', $this->data, true);
				$this->pdfgenerator->generateInvoice($html, $pdflink, 'A4', 'Landscape', false);
			} else {
				$this->data['error'] = 'Unable to update order status';
			}

		}
		$this->load->view('dashboard/vinvoice', $this->data);
	}
	public function products() {
		/* add product */
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserId($data['id']);

		$this->form_validation->set_rules('pname', 'Product', 'trim|required');
		$this->form_validation->set_rules('pricekos', 'Price', 'trim|required|numeric');

		if ($this->form_validation->run()) {
			$data = array(
				'produk' => $this->input->post('pname'),
				'hargakos' => sprintf('%0.2f', $this->input->post('pricekos')),
				'unit' => 0,
				'user' => $user,
				'date' => date('Y-m-d'),
				'time' => time(),
			);
			if ($this->mdashboard->insProduct($data)) {
				$this->data['insert_success'] = 'New product successfully added';
			} else {
				$this->data['insert_error'] = 'Unable to add new product. Please try again or contact admin.';
			}
		}
		/* end of add product */

		$this->data['total_items'] = $this->mdashboard->countProduct();
		$this->data['products'] = $this->mdashboard->getProductByLimit($this->custompaging->per_page, $this->custompaging->offset);
		$this->data['paging'] = $this->custompaging->get_pagination($this->data['total_items'], $this->custompaging->per_page);

		$this->load->view('dashboard/vproducts', $this->data);
	}
	public function stocks() {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserId($data['id']);
		$all = $this->mdashboard->getProductAll();
		$this->data['lists'] = $all;

		/* view logstock */
		if ($this->input->post('produkn') == "") {
			$a = $this->mdashboard->countLogStockAll($user);
			$this->data['loglist'] = $this->mdashboard->getLogStockAll($user, $a);
		} else {
			$pid = $this->input->post('produkn');
			$a = $this->mdashboard->countLogStockbyProduct($user, $pid);
			$this->data['loglist'] = $this->mdashboard->getLogStockbyProduct($user, $pid, $a);
		}
		/* end view logstock */
		$this->load->view('dashboard/vstocks', $this->data);
	}
	public function editStock($id) {
		$product = $this->mdashboard->getProductbyId($id);
		$this->data['product'] = $product;

		$this->form_validation->set_rules('pname', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('pricekos', 'Cost Price', 'trim|required|numeric');
		if ($this->form_validation->run()) {
			$data = array(
				'produk' => $this->input->post('pname'),
				'hargakos' => $this->input->post('pricekos'),
			);
			if ($this->mdashboard->updateProduct($data, $product->id)) {
				$this->data['update_success'] = 'Product has been change to ' . $this->input->post('pname') . ' & price has been change to ' . $this->input->post('pricekos') . '.';
			} else {
				$this->data['update_error'] = 'Unable to update product name and price. Please try again.';
			}
		}
		$this->load->view('dashboard/veditstock', $this->data);
	}
	public function addStock() {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserId($data['id']);
		/* start update stock */
		$all = $this->mdashboard->getProductAll();
		$this->data['lists'] = $all;

		$this->form_validation->set_rules('produk', 'Product', 'required');
		$this->form_validation->set_rules('unit', 'Quantity', 'required');
		$this->form_validation->set_rules('type', 'Option', 'required');

		if ($this->form_validation->run()) {
			$type = $this->input->post('type');
			$id = $this->input->post('produk');
			$newUnit = $this->input->post('unit');
			$prevUnit = $this->mdashboard->getProductUnit($id);
			$add = (($prevUnit->unit) + $newUnit);
			$sub = (($prevUnit->unit) - $newUnit);
			if ($type == 1) {
				$data = array(
					'unit' => $add,
				);
				if ($this->mdashboard->updateStock($data, $id)) {
					$data2 = array(
						'last' => $prevUnit->unit,
						'new' => $add,
						'type' => 1,
						'produk' => $id,
						'time' => time(),
						'referrer' => $user,
						'date' => date('Y-m-d'),
					);
					if ($this->mdashboard->insertLogStock($data2)) {
						$this->data['update_success'] = 'Item ' . $id . ' has been logged.';
					} else {
						$this->data['update_error'] = 'Unable to log item ' . $id;
					}
					$this->data['update_success'] = 'Item ' . $id . ' successfully updated with quantity ' . $add . ' units.';
				} else {
					$this->data['update_error'] = 'Unable to update item. Please try again.';
				}
			} else if ($type == 2) {
				$data = array(
					'unit' => $sub,
				);
				if ($this->mdashboard->updateStock($data, $id)) {
					$data2 = array(
						'last' => $prevUnit->unit,
						'new' => $sub,
						'type' => 2,
						'produk' => $id,
						'time' => time(),
						'referrer' => $user,
						'date' => date('Y-m-d'),
					);
					if ($this->mdashboard->insertLogStock($data2)) {
						$this->data['update_success'] = 'Item ' . $id . ' has been logged.';
					} else {
						$this->data['update_error'] = 'Unable to log item ' . $id;
					}
					$this->data['update_success'] = 'Item ' . $id . ' successfully updated with quantity ' . $sub . ' units.';
				} else {
					$this->data['update_error'] = 'Unable to update item. Please try again.';
				}
			} else if ($type == 3) {
				$data = array(
					'unit' => $prevUnit->unit,
				);
				if ($this->mdashboard->updateStock($data, $id)) {
					$data2 = array(
						'last' => $prevUnit->unit,
						'new' => $prevUnit->unit,
						'type' => 3,
						'produk' => $id,
						'time' => time(),
						'referrer' => $user,
						'date' => date('Y-m-d'),
					);
					if ($this->mdashboard->insertLogStock($data2)) {
						$this->data['update_success'] = 'Item ' . $id . ' has been logged.';
					} else {
						$this->data['update_error'] = 'Unable to log item ' . $id;
					}
					$this->data['update_success'] = 'Item ' . $id . ' does not have update.';
				} else {
					$this->data['update_error'] = 'Unable to update item. Please try again.';
				}
			}
		}
		/* end update stock */
		$this->load->view('dashboard/vaddstock', $this->data);
	}
	public function remove($id) {
		if ($this->mdashboard->removeProduct($id)) {
			$msg = 'Item ' . $id . ' has been deleted';
			$this->session->set_tempdata('success', $msg, 5);
			redirect('/dashboard/products', 'refresh');
		} else {
			$msg = 'Item deleted failed. Please try later.';
			$this->session->set_tempdata('error', $msg, 5);
			redirect('/dashboard/products', 'refresh');
		}
	}
	public function forms() {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserId($data['id']);
		$this->data['products'] = $this->mdashboard->getProduct($user);
		$this->data['banks'] = $this->mdashboard->getBank();
		$this->data['states'] = $this->mdashboard->getStates();

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('add1', 'Address', 'trim|required');
		$this->form_validation->set_rules('add2', 'Address', 'trim');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required|numeric');
		$this->form_validation->set_rules('cities', 'City', 'trim|required');
		$this->form_validation->set_rules('states', 'State', 'required');
		$this->form_validation->set_rules('product', 'Products', 'required');
		$this->form_validation->set_rules('unit', 'Unit', 'required');
		$this->form_validation->set_rules('bank', 'Bank', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('times', 'Time', 'required');

		if ($this->form_validation->run()) {
			if (!is_dir('uploads/resit/')) {
				mkdir('uploads/resit/', 0777, true);
			}
			$config['upload_path'] = './uploads/resit/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
			$config['file_name'] = date('Ymd') . '-' . mt_rand();
			$config['file_ext_tolower'] = true;
			$config['max_size'] = 5000;
			$config['encrypt_name'] = true;
			$config['remove_spaces'] = true;

			$this->upload->initialize($config);
			if (!$this->upload->do_upload('receipt')) {
				$this->data['upload_error'] = array('error' => $this->upload->display_errors('<p>', '</p>'));
			} else {
				$pdf = date('Ymd') . '_' . $this->input->post('phone') . '.pdf';
				$this->upload->data();

				/*$regex1 = '/([0-9].+)\s?/';
				$regex2 = '/([a-zA-Z].+)\s?/';*/
				$str = $this->input->post('states');
				/*preg_match($regex1, $str, $states_id);
				preg_match($regex2, $str, $states_name);*/
				if ($str == '12' || $str == '13' || $str == '15') {
					$postage = 2;
				} else {
					$postage = 1;
				}
				$data = array(
					'name' => $this->input->post('name'),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'add1' => strtoupper($this->input->post('add1')),
					'add2' => strtoupper($this->input->post('add2')),
					'postcode' => strtoupper($this->input->post('postcode')),
					'city' => strtoupper($this->input->post('cities')),
					'states_id' => $str,
					'product' => $this->input->post('product'),
					'unit' => $this->input->post('unit'),
					'bank' => $this->input->post('bank'),
					'bankdate' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
					'banktime' => $this->input->post('times'),
					'resit' => $this->upload->data('file_name'),
					'pdflink' => $pdf,
					'price' => $this->input->post('price'),
					'referrer' => $user,
					'sellerid' => $data['id'],
					'time' => time(),
					'postage' => $postage,
					'date' => date('Y-m-d'),
					'ip' => $this->systools->getIP(),
					'info' => $this->input->post('notes'),
				);
				$id = $this->input->post('product');
				$prevUnit = $this->mdashboard->getProductUnit($id);
				$newUnit = $this->input->post('unit');
				$change = (($prevUnit->unit) - $newUnit);
				$changes = array(
					'unit' => $change,
				);
				if ($this->mdashboard->updateStock($changes, $id)) {
					$rslt = array(
						'last' => $prevUnit->unit,
						'new' => $change,
						'type' => 4,
						'produk' => $id,
						'time' => time(),
						'referrer' => $user,
						'date' => date('Y-m-d'),
					);
					$this->mdashboard->insertLogStock($rslt);
				}
				if ($this->mdashboard->insOrder($data)) {
					$this->data['insert_success'] = 'Your order have been submitted';
				} else {
					$this->data['insert_error'] = 'New order failed to submit. Please try again.';
				}
			}
		}
		$this->load->view('dashboard/vforms', $this->data);
	}
	public function sales() {
		$this->data['today'] = $this->mdashboard->getTodaySales(date('Y-m-d'));
		$this->data['month'] = $this->mdashboard->getMonthSales(date('m'));
		$this->data['year'] = $this->mdashboard->getYearlySales(date('Y'));
		$this->data['total'] = $this->mdashboard->getTotalSales();
		$this->load->view('dashboard/vsales', $this->data);
	}
	public function tracking() {

		$this->data['total_items'] = $this->mdashboard->countOrdersAll();
		$this->data['order'] = $this->mdashboard->getOrdersbyTime($this->custompaging->per_page, $this->custompaging->offset);
		$this->data['paging'] = $this->custompaging->get_pagination($this->data['total_items'], $this->custompaging->per_page);

		$trackingno = strtoupper($this->input->post('trackingno'));
		$orderid = $this->input->post('oid');
		$trackstatus = $this->input->post('trackstatus');
		$edit = $this->input->post('edit');

		if ($trackingno != '' && $edit == '') {
			$data = array(
				'trackingno' => $trackingno,
				'status' => $trackstatus,
				'trackingdate' => date('Y-m-d'),
			);

			if ($this->mdashboard->updateTracking($data, $orderid)) {
				$this->data['success'] = 'Order have been update with tracking no ' . $trackingno;
				header("Refresh:1, url=tracking");
			} else {
				$this->data['error'] = 'Unable to update tracking no. Please try again';
			}
		} else if ($trackingno == '' && $edit != '') {
			$data = array(
				'trackingno' => '',
				'status' => $edit,
				'trackingdate' => '',
			);
			if ($this->mdashboard->updateTracking($data, $orderid)) {
				$this->data['success'] = 'Please input new tracking no!';
				header("Refresh:1, url=tracking");
			} else {
				$this->data['error'] = 'Unable to process. Please try again';
			}
		}

		$this->load->view('dashboard/vtracking', $this->data);
	}
	public function tools() {
		/*orders*/
		$ostartdate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('ostartdate'))));
		$oenddate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('oenddate'))));
		$oExcel = $this->input->post('o1');
		$oPdf = $this->input->post('o2');

		if ($oExcel != '') {
			$data = $this->session->userdata('user');
			$creator = $data['username'];
			$title = 'Orders ' . date('d M Y', strtotime($ostartdate)) . ' - ' . date('d M Y', strtotime($oenddate));
			$sheetname = date('dmY', strtotime($ostartdate)) . ' - ' . date('dmY', strtotime($oenddate));
			$date = date('d M Y', strtotime($ostartdate)) . ' - ' . date('d M Y', strtotime($oenddate));
			$result = $this->mdashboard->backupOrders($ostartdate, $oenddate);
			$filename = 'Backup-Orders-' . date('dmY') . '-' . date('hiA') . '.xlsx';
			if ($this->excelgenerator->generateOrders($creator, $title, $sheetname, $date, $result, $filename)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/excel';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		} else if ($oPdf != '') {
			$this->data['startdate'] = $ostartdate;
			$this->data['enddate'] = $oenddate;
			$rslt = $this->mdashboard->backupOrders($ostartdate, $oenddate);
			$this->data['result'] = $rslt;
			$link = 'Backup-Orders-' . date('dmY') . '-' . date('hiA') . '.pdf';
			$html = $this->load->view('vbackuporders', $this->data, true);
			if ($this->pdfgenerator->generateBackup($html, $link, 'A4', 'Landscape', false)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/backup';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		}
		/* end of orders */
		/*sales*/
		$sstartdate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('sstartdate'))));
		$senddate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('senddate'))));
		$sExcel = $this->input->post('s1');
		$sPdf = $this->input->post('s2');

		if ($sExcel != '') {
			$data = $this->session->userdata('user');
			$creator = $data['username'];
			$title = 'Sales ' . date('d M Y', strtotime($sstartdate)) . ' - ' . date('d M Y', strtotime($senddate));
			$sheetname = date('dmY', strtotime($sstartdate)) . ' - ' . date('dmY', strtotime($senddate));
			$date = date('d M Y', strtotime($sstartdate)) . ' - ' . date('d M Y', strtotime($senddate));
			$result = $this->mdashboard->backupSales($sstartdate, $senddate);
			$filename = 'Backup-Sales-' . date('dmY') . '-' . date('hiA') . '.xlsx';
			if ($this->excelgenerator->generateSales($creator, $title, $sheetname, $date, $result, $filename)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/excel';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		} else if ($sPdf != '') {
			$this->data['startdate'] = $sstartdate;
			$this->data['enddate'] = $senddate;
			$rslt = $this->mdashboard->backupSales($sstartdate, $senddate);
			$this->data['result'] = $rslt;
			$link = 'Backup-Sales-' . date('dmY') . '-' . date('hiA') . '.pdf';
			$html = $this->load->view('vbackupsales', $this->data, true);
			if ($this->pdfgenerator->generateBackup($html, $link, 'A4', 'Landscape', false)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/backup';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		}
		/* end of sales*/
		/*products*/
		$pstartdate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('pstartdate'))));
		$penddate = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('penddate'))));
		$pExcel = $this->input->post('p1');
		$pPdf = $this->input->post('p2');

		if ($pExcel != '') {
			$data = $this->session->userdata('user');
			$creator = $data['username'];
			$title = 'Product ' . date('d M Y', strtotime($pstartdate)) . ' - ' . date('d M Y', strtotime($penddate));
			$sheetname = date('dmY', strtotime($pstartdate)) . ' - ' . date('dmY', strtotime($penddate));
			$date = date('d M Y', strtotime($pstartdate)) . ' - ' . date('d M Y', strtotime($penddate));
			$result = $this->mdashboard->backupProduct($pstartdate, $penddate);
			$filename = 'Backup-Products-' . date('dmY') . '-' . date('hiA') . '.xlsx';
			if ($this->excelgenerator->generateProduct($creator, $title, $sheetname, $date, $result, $filename)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/excel';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		} else if ($pPdf != '') {
			$this->data['startdate'] = $pstartdate;
			$this->data['enddate'] = $penddate;
			$rslt = $this->mdashboard->backupProduct($pstartdate, $penddate);
			$this->data['result'] = $rslt;
			$link = 'Backup-Products-' . date('dmY') . '-' . date('hiA') . '.pdf';
			$html = $this->load->view('vbackupproduct', $this->data, true);
			if ($this->pdfgenerator->generateBackup($html, $link, 'A4', 'Landscape', false)) {
				$this->data['success'] = 'Backup have been generated. Please find your backup at /uploads/backup';
				header("Refresh: 2; url=tools");
			} else {
				$this->data['error'] = 'Unable to generate backup. Please try again';
			}
		}
		/* end of products*/
		$this->load->view('dashboard/vtools', $this->data);
	}
	public function setting() {
		$data = $this->session->userdata('user');
		$user = $this->musers->getUserInfo($data['id']);
		$states = $this->mdashboard->getStates();
		$this->data['states'] = $states;
		$this->data['user'] = $user;

		/* sms */
		$smsname = $this->input->post('smsname');
		$smspass = $this->input->post('smspass');
		$smsbtn = $this->input->post('smsbtn');
		if ($smsbtn == 1) {
			$arrdata = array(
				'smsname' => $smsname,
				'smspass' => $smspass,
			);
			if ($this->musers->updateSMSDetails($arrdata, $data['id'])) {
				$this->data['success'] = 'Successfully update SMS Gateway details';
				header("Refresh: 2, url=setting");
			} else {
				$this->data['error'] = 'Unable to update data. Please try again';
			}
		}
		/* end of sms */
		/*  details */
		$username = $this->input->post('username');
		$companyno = $this->input->post('companyNo');
		$gstno = $this->input->post('gstNo');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$gstrate = $this->input->post('gstrate');
		$add1 = $this->input->post('add1');
		$postcode = $this->input->post('postcode');
		$city = $this->input->post('cities');
		$state = $this->input->post('states');
		$detailsbtn = $this->input->post('detailsbtn');

		if ($detailsbtn == 1) {
			$arrdata = array(
				'username' => $username,
				'company' => $companyno,
				'gst' => $gstno,
				'email' => $email,
				'phone' => $phone,
				'gstrate' => $gstrate,
				'address' => $add1,
				'poskod' => $postcode,
				'town' => $city,
				'state' => $state,
			);
			if ($this->musers->updateUserDetails($arrdata, $data['id'])) {
				$this->data['success'] = 'Successfully update details';
				header("Refresh: 2, url=setting#account");
			} else {
				$this->data['error'] = 'Unable to update. Please try again';
			}
		}
		/* end of details*/
		/* change password */
		$this->form_validation->set_rules('current', 'Current Password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required');
		$this->form_validation->set_rules('cnew', 'Retype Password', 'required|matches[new]');

		if ($this->form_validation->run()) {
			$arrdata = array(
				'password' => $this->musers->saltedHash($this->input->post('new')),
			);
			if ($user = $this->musers->checkUser($data['username'])) {
				if ($this->musers->verifyPassword($this->input->post('current'), $user['password'])) {
					if ($this->musers->updateUserDetails($arrdata, $data['id'])) {
						$this->data['success'] = 'Password change successfully';
						header("Refresh:2, url=setting#password");
					} else {
						$this->data['error'] = 'Unable to change new password. Please try again';
					}
				} else {
					$this->data['error'] = 'Current password not match';
				}
			}
		}
		/* end of change password*/
		/* change photo */
		$photobtn = $this->input->post('photobtn');
		if ($photobtn == 1) {
			if (!is_dir('uploads/profile/')) {
				mkdir('uploads/profile/', 0777, true);
			}

			$config['upload_path'] = './uploads/profile/';
			$config['allowed_types'] = 'jpg|jpeg|png';
			$config['file_name'] = $data['username'] . '_' . date('Ymd') . '-' . mt_rand();
			$config['file_ext_tolower'] = true;
			$config['max_size'] = 5000;
			$config['encrypt_name'] = false;
			$config['remove_spaces'] = true;

			$this->upload->initialize($config);
			if (!$this->upload->do_upload('photo')) {
				$this->data['upload_error'] = array('error' => $this->upload->display_errors('<p>', '</p>'));
			} else {
				$this->upload->data();

				$arrdata = array(
					'picture' => $this->upload->data('file_name'),
				);
				if ($this->musers->updateUserDetails($arrdata, $data['id'])) {
					$this->data['success'] = 'New photo updated';
					header("Refresh:2, url=setting#system");
				} else {
					$this->data['error'] = 'Unable to upload new photo. Please try again';
				}
			}
		}
		/* end of change photo */
		/* store name */
		$storename = $this->input->post('storename');
		$storebtn = $this->input->post('storebtn');

		if ($storebtn == 1) {
			$arrdata = array(
				'name' => $storename,
			);
			if ($this->musers->updateUserDetails($arrdata, $data['id'])) {
				$this->data['success'] = 'Store name successfully changed to ' . $storename;
				header("Refresh:2, url=setting");
			} else {
				$this->data['error'] = 'Unable to change request. Please try again';
			}
		}
		/* end of store name */
		$this->load->view('dashboard/vsetting', $this->data);
	}
}