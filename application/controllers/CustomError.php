<?php defined('BASEPATH') OR exit('No direct script access allowed');

class customError extends CI_Controller {
	function __construct() {
		parent::__construct();
	}
	public function error404() {
		$this->load->view('error_404');
	}
	public function error500() {
		$this->load->view('error_500');
	}
}