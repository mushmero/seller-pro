<?php defined('BASEPATH') OR exit('No direct script access allowed');

define('WEBNAME', 'Pro-Seller'); //website name
define('DBHOST', 'localhost'); //database host or ip address
define('DBUSER', 'root'); //database username
define('DBPASS', ''); //database password
define('DBNAME', 'proseller'); //database name
define('WEBLOGO', base_url('/assets/icons/favicon.png'));
define('PDFLOGO', $_SERVER['DOCUMENT_ROOT'] . 'assets/icons/favicon.png');
define('mailhost', '');
define('mailport', '');
define('mailuser', '');
define('mailpass', '');
