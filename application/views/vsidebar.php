<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(2);?>
<?php $data = $this->session->userdata('user');?>
<?php $user = $this->musers->getUserInfo($data['id']);?>
<aside class="main-sidebar">
	<section class="sidebar">
      <!-- Sidebar user panel -->
      	<div class="user-panel">
        	<div class="pull-left image">
          		<img src="<?php if (is_array($user['picture'])) {echo base_url('/uploads/profile/') . $user['picture'];} else {echo base_url('/assets/dist/img/avatar04.png');}?>" class="img-circle" alt="User Image">
        	</div>
        	<div class="pull-left info">
          		<p><?php echo $data['username']; ?></p>
          		<a href="#"><i class="fas fa-circle text-success"></i> Online</a>
        	</div>
	    	<div class="logout-panel">
	    		<div class="pull-left logout-button">
	    			<a href="<?php echo site_url('logout'); ?>" class="btn btn-flat btn-block btn-danger"><i class="fas fa-sign-out-alt text-white"></i> <span class="text-white">Logout</span></a>
	    		</div>
    		</div>
     	</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header bg-blue">Navigation</li>
			<?php if ($uri == 'dashboard' || $uri == '') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a>
			</li>
			<?php if ($uri == 'products') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/products"><i class="fas fa-cubes"></i> <span>Products</span></a>
			</li>
			<?php if ($uri == 'orders' || $uri == 'forms') {?>
			<li class="active treeview">
			<?php } else {?>
			<li class="treeview">
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/orders">
					<i class="fas fa-eye"></i> <span>Orders</span>
					<span class="pull-right-container">
		              <i class="fas fa-angle-left pull-right"></i>
		            </span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo site_url('dashboard'); ?>/orders"><i class="fas fa-list-ol"></i> List Orders</a></li>
					<li><a href="<?php echo site_url('dashboard'); ?>/forms"><i class="fas fa-edit"></i> Submit Order</a></li>
				</ul>
			</li>
			<?php if ($uri == 'sales') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/sales"><i class="fas fa-th-list"></i> <span>Sales</span></a>
			</li>
			<?php if ($uri == 'trackingno') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/tracking"><i class="fas fa-truck"></i> <span>Tracking</span></a>
			</li>
			<?php if ($uri == 'tools') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/tools"><i class="fas fa-terminal"></i> <span>Tools</span></a>
			</li>
			<?php if ($uri == 'setting') {?>
			<li class="active">
			<?php } else {?>
			<li>
			<?php }?>
				<a href="<?php echo site_url('dashboard'); ?>/setting"><i class="fas fa-cog"></i> <span>Settings</span></a>
			</li>
		</ul>
	</section>
</aside>
<!-- end of sidebar -->