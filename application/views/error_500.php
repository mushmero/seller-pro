<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('vheader');?>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
          <p>We will work on fixing that right away. You may return to <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a> and start again.</p>
        </div>
      </div>
      <!-- /.error-page -->
    </section>

<?php $this->load->view('vfooter');?>