<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report Sales</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Report Sales">
    <meta name="author" content="mushmero">
    <link rel="icon" type="image/x-icon" href="<?php echo WEBLOGO; ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header with-border">
    					<div class="text-center">
    						<h2>Report Sales <br>
    						<strong><?php echo date('d M Y', strtotime($startdate)) . ' - ' . date('d M Y', strtotime($enddate)); ?></strong></h2>
    					</div>
    				</div>
    				<div class="box-body no-padding">
    					<table class="table table-condensed" width="3508">
    						<tr>
    							<th class="col-xs-2 text-center bg-red">Product Cost</th>
    							<th class="col-xs-2 text-center bg-red">Postage Cost</th>
    							<th class="col-xs-2 text-center bg-red">Ads Cost</th>
    							<th class="col-xs-3 text-center bg-yellow">Total Cost</th>
    							<th class="col-xs-3 text-center bg-aqua">Total Sales</th>
                                <th class="col-xs-3 text-center bg-green">Nett Profit</th>
    						</tr>
    						<?php if (!empty($result)) {?>
                                <tr>
                                    <td>RM<?php echo sprintf('%0.2f', $result[0]->productcost); ?></td>
                                    <td>RM<?php echo sprintf('%0.2f', $result[0]->postagecost); ?></td>
                                    <td>RM<?php echo sprintf('%0.2f', $result[0]->adscost); ?></td>
                                    <td>RM<?php echo sprintf('%0.2f', ($result[0]->productcost + $result[0]->postagecost + $result[0]->adscost)); ?></td>
                                    <td>RM<?php echo sprintf('%0.2f', $result[0]->totalsales); ?></td>
                                    <td>RM<?php echo sprintf('%0.2f', $result[0]->totalsales - ($result[0]->productcost + $result[0]->postagecost + $result[0]->adscost)) ?></td>
                                </tr>
    						<?php } else {?>
    							<tr>
    								<td colspan="6">
    									<div class="text-center">
    										<h3><strong>Sorry! No information found. Please try again.</strong></h3>
    									</div>
    								</td>
    							</tr>
    						<?php }?>
    					</table>
    				</div>
    				<div class="box-footer"></div>
    			</div>
    		</div>
    	</div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('/assets/dist/js/bootstrap-clockpicker.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/dist/js/pages/core.js'); ?>"></script>
<script src="<?php echo base_url('/assets/plugins/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/plugins/fastclick/fastclick.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/dist/js/app.min.js'); ?>"></script>
</body>
</html>