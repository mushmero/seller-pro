<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('vheader');?>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3 class="text-yellow"><i class="fas fa-exclamation-triangle"></i> Oh no! Page not found.</h3>

          <p class="text-white">We could not find the page you were looking for. You may return to <a href="<?php echo site_url(); ?>"><?php echo site_url(); ?></a> and start again.</p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
<?php $this->load->view('vfooter');?>