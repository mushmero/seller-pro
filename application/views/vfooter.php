<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(1);?>

<?php if ($uri == "" || $uri == "login" || $uri == "forgot" || $uri == "reset" || $uri == "logout") {?>
<footer class="login-footer">
	<?php $color = "text-white";?>
<?php } else {?>
<footer class="main-footer">
	<?php $color = "text-muted";?>
<?php }?>
	<p class="text-center <?php echo $color ?>"><strong>Copyright &copy; <?php echo date('Y'); ?></strong> <?php echo WEBNAME; ?> . Developed by <a href="http://github.com/mushmero" target="_blank">mushmero</a>. All rights reserved.</p>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('/assets/dist/js/bootstrap-clockpicker.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
<script src="//www.tracking.my/track-button.js"></script>
<script src="<?php echo base_url('/assets/dist/js/adminlte.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/dist/js/core.js'); ?>"></script>
</div>
</body>
</html>