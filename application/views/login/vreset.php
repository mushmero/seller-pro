<?php
$this->load->view('vheader');
$token_error = (trim(form_error('token')) != '') ? ' error' : '';
$password_error = (trim(form_error('password')) != '') ? ' error' : '';
$cpassword_error = (trim(form_error('cpassword')) != '') ? ' error' : '';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">&nbsp;</div>
		<div class="col-md-4">
			<div class="login-box">
			  <div class="login-logo">
			    <a href="/"><b><?php echo $_SERVER['HTTP_HOST']; ?> <img src="<?php echo WEBLOGO; ?>"></b></a>
			  </div>
			<div class="login-box-body">
				<p class="login-box-msg">Reset Password</p>
				<form method="POST" action="<?php echo site_url() . 'reset/token/' . $token; ?>">
					<?php echo (isset($update_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_error</strong></div>" : ''; ?>
					<?php echo (isset($update_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_success</strong></div>" : ''; ?>
					<div class="<?php echo $token_error; ?>">&nbsp;</div>
					<?php if (!isset($update_success)) {?>
					<div class="form-group<?php echo $password_error; ?>">
				      <label class="control-label" for="password">Password</label>
				        <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>">
						<?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
					</div>
				    <div class="control-group<?php echo $cpassword_error; ?>">
				      <label class="control-label" for="cpassword">Confirm Password</label>
				        <input type="password" class="form-control" id="cpassword" name="cpassword" value="<?php echo set_value('cpassword'); ?>">
						<?php echo form_error('cpassword', '<p class="help-inline">', '</p>'); ?>
					</div>
					<div class="form-group">
				        <div class="col-xs-12">
				    		<button type="submit" class="btn btn-primary btn-block">Reset</button>
				        </div>
				    </div>
				    <?php }?>
				</form>
			</div>
		</div>
		<div class="col-md-4">&nbsp;</div>
	</div>
</div>
</div>
<?php $this->load->view('vfooter');?>