<?php
$this->load->view('vheader');
$username_error = (trim(form_error('username')) != '') ? ' error' : '';
$password_error = (trim(form_error('password')) != '') ? ' error' : '';
?>

<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo site_url(); ?>"><b><?php echo WEBNAME; ?></b> <img src="<?php echo WEBLOGO; ?>"></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><span class="fas fa-sign-in-alt"></span> Login Panel</p>
        <form method="POST" action="<?php echo site_url(); ?>">
            <?php echo (isset($login_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$login_success</strong></div>" : ''; ?>
            <?php echo (isset($login_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$login_error</strong></div>" : ''; ?>

            <div class="form-group has-feedback <?php echo $username_error; ?>">
                <input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="Username">
                <i class="glyphicon glyphicon-user form-control-feedback"></i>
                <?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
            </div>
            <div class="form-group has-feedback <?php echo $password_error; ?>">
                <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Password">
                <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                <?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
            </div>
            <div class="col-xs-12">
                <button type="submit" name="login" class="btn btn-primary btn-block">Login</button>
            </div>
        </form>
        <div class="forgot-password">
            <a href="<?php echo site_url('forgot') ?>">Forgot Password?</a>
        </div>
    </div>
</div>

<?php $this->load->view('vfooter');?>