<?php
$this->load->view('vheader');
$email_error = (trim(form_error('email')) != '') ? ' error' : '';
?>
<div class="login-box">
  	<div class="login-logo">
		<a href="<?php echo site_url(); ?>"><b><?php echo WEBNAME ?> <img src="<?php echo WEBLOGO; ?>"></b></a>
	</div>
	<div class="login-box-body">
		<p class="login-box-msg"><span class="fas fa-user-lock"></span> Forgot Password</p>
		<form method="POST" action="<?php echo site_url('forgot'); ?>">
			<p>Please enter your registered email and we'll be get back to you</p>
			<?php echo (isset($reset_button)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$reset_button</strong></div>" : ''; ?>
			<div class="form-group <?php echo $email_error; ?>">
				<input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email Address">
				<?php echo form_error('email', '<p class="help-inline">', '</p>'); ?>
			</div>
			<div class="col-xs-12">
		    	<button type="submit" name="forgot" class="btn btn-primary btn-block">Submit</button>
		    </div>
		</form>
	</div>
</div>
<?php $this->load->view('vfooter');?>