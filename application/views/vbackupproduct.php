<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report Products</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Report Products">
    <meta name="author" content="mushmero">

    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="icon" type="image/x-icon" href="<?php echo WEBLOGO; ?>">
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header with-border">
    					<div class="text-center">
    						<h2>Report Products <br>
    						<strong><?php echo date('d M Y', strtotime($startdate)) . ' - ' . date('d M Y', strtotime($enddate)); ?></strong></h2>
    					</div>
    				</div>
    				<div class="box-body no-padding">
    					<table class="table table-bordered table-condensed" width="3508">
    						<tr>
    							<th class="text-center col-xs-1">#</th>
    							<th class="col-xs-3">Product</th>
    							<th class="col-xs-3">Unit</th>
    							<th class="col-xs-3">Cost Price</th>
    						</tr>
    						<?php if (is_array($result)) {?>
    						<?php $i = 1;foreach ($result as $r) {?>
    						<tr>
    							<td class="text-center"><?php echo $i; ?></td>
    							<td><?php echo $r->produk; ?></td>
    							<td><?php echo $r->unit; ?></td>
    							<td>RM<?php echo sprintf('%0.2f', $r->hargakos); ?></td>
    						</tr>
    						<?php $i++;}?>
    						<?php } else {?>
    							<tr>
    								<td colspan="4">
    									<div class="text-center">
    										<h3><strong>Sorry! No information found. Please try again.</strong></h3>
    									</div>
    								</td>
    							</tr>
    						<?php }?>
    					</table>
    				</div>
    				<div class="box-footer"></div>
    			</div>
    		</div>
    	</div>
    </div>
</body>
</html>