<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-edit"></i> Edit Product</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Edit Product</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php echo (isset($update_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_success</strong></div>" : ''; ?>
  				<?php echo (isset($update_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_error</strong></div>" : ''; ?>
			</div>
		</div>
	  	<div class="row">
	   		<div class="col-md-12">
	      		<div class="box">
	        		<div class="box-header with-border">
	        			<h3 class="box-title">Product Information</h3>
	        		</div>
	        		<div class="box-body">
	        			<form action="<?php echo site_url('dashboard') ?>/products/edit/<?php echo $product->id; ?>" id="addform" class="form-horizontal" method="POST">
	        				<div class="form-group">
	        					<label class="col-sm-2 control-label">Product Name</label>
	        					<div class="col-sm-10">
	        						<input type="text" name="pname" class="form-control" value="<?php echo $product->produk; ?>" placeholder="Product Name">
	        						<?php echo form_error('pname', '<p class="help-inline">', '</p>'); ?>
	        					</div>
	        				</div>
	        				<div class="form-group">
	        					<label class="col-sm-2 control-label">Cost Price (RM)</label>
	        					<div class="col-sm-10">
	        						<input type="number" name="pricekos" class="form-control" value="<?php echo $product->hargakos; ?>" placeholder="Cost Price" step="any">
	        						<?php echo form_error('pricekos', '<p class="help-inline">', '</p>'); ?>
	        					</div>
	        				</div>
	        				<div class="box-footer">
	        					<div class="row">
	        						<div class="col-sm-offset-2 col-sm-10">
	        							<button type="submit" form="addform" class="btn btn-primary" name="edit">Update</button>
	        						</div>
	        					</div>
	        				</div>
	        			</form>
	        		</div>
	        	</div>
	        </div>
	    </div>
	    <div class="row">
	    	<div class="col-xs-12">
	    		<h1 class="text-center"><a href="<?php echo site_url('dashboard'); ?>/products"><i class="fas fa-arrow-circle-left"></i> Back</a></h1>
	    	</div>
	    </div>
	</section>
</div>

<?php $this->load->view('vfooter');?>