<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-cubes"></i> Product List</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Product List</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($insert_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$insert_success</strong></div>" : ''; ?>
				<?php echo (isset($insert_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$insert_error</strong></div>" : ''; ?>
			</div>
		</div>
	  	<div class="row">
	   		<div class="col-md-12">
	      		<div class="box box-info">
	        		<div class="box-header with-border">
	        			<h3 class="box-title">Add Product</h3>
	        		</div>
	        		<div class="box-body">
	        			<form action="<?php echo site_url('dashboard'); ?>/products" id="addform" class="form-horizontal" method="POST">
	        				<div class="form-group">
	        					<label class="col-sm-2 control-label">Product Name</label>
	        					<div class="col-sm-10">
	        						<input type="text" name="pname" class="form-control" value="<?php echo set_value('pname'); ?>" placeholder="Product Name">
	        						<?php echo form_error('pname', '<p class="help-inline">', '</p>'); ?>
	        					</div>
	        				</div>
	        				<div class="form-group">
	        					<label class="col-sm-2 control-label">Cost Price (RM)</label>
	        					<div class="col-sm-10">
	        						<input type="number" name="pricekos" class="form-control" value="<?php echo set_value('pricekos'); ?>" placeholder="Cost Price" step="any">
	        						<?php echo form_error('pricekos', '<p class="help-inline">', '</p>'); ?>
	        					</div>
	        				</div>
	        				<div class="box-footer">
	        					<div class="row">
	        						<div class="col-sm-offset-2 col-sm-10">
	        							<button type="submit" class="btn btn-primary">Add <i class="fas fa-plus"></i></button>
	        						</div>
	        					</div>
	        				</div>
	        			</form>
	        		</div>
	        	</div>
	        </div>
	        <div class="col-md-12">
	            <div class="box box-warning">
	            	<div class="box-header with-border">
	            		<h3 class="box-title">Product List</h3>
	            		<div class="pull-right">
	            			<div class="row">
		            			<div class="col-xs-6">
			            			<a href="<?php echo site_url('dashboard'); ?>/products/stock" ><button class="btn btn-primary"><i class="fa fa-list"></i> LogStock</button></a>
			            		</div>
			            		<div class="col-xs-6">
			            			<a href="<?php echo site_url('dashboard'); ?>/products/addstock" ><button class="btn btn-primary"><i class="fa fa-plus"></i> Edit Stock</button></a>
			            		</div>
		            		</div>
	            		</div>
	            	</div>
	            	<div class="box-body table-responsive no-padding">
	            		<table class="table table-hover">
	            			<tr>
	            				<th class="text-center"> # </th>
	            				<th width="50%"> Product </th>
	            				<th class="text-left"> Cost Price </th>
	            				<th class="text-center"> Available Stock </th>
	            				<th class="text-center"> Action </th>
	            			</tr>
	            			<?php if (is_array($products)) {?>
	            			<?php $i = $this->uri->segment(4) + 1;foreach ($products as $p) {?>
	            			<tr>
	            				<td class="text-center"> <?php echo $i; ?></td>
	            				<td><a href="<?php echo site_url('dashboard'); ?>/products/stock/<?php echo $p->id; ?>"><?php echo $p->produk; ?></a></td>
	            				<td class="text-left"> RM <?php echo sprintf('%0.2f', $p->hargakos); ?></td>
	            				<td class="text-center"> <?php echo $p->unit ?> unit</td>
	            				<td class="text-center">
	            					<div class="btn-group">
	            						<a href="<?php echo site_url('dashboard'); ?>/products/edit/<?php echo $p->id; ?>"><button class="btn btn-block btn-info btn-flat" title="Edit product"><i class='fa fa-edit'></i></button></a>
	            					</div>
	            					<div class="btn-group">
	            						<a href="<?php echo site_url('dashboard'); ?>/products/remove/<?php echo $p->id; ?>"><button class="btn btn-block btn-danger btn-flat" title="Remove product"><i class="fa fa-times"></i></button></a>
	            					</div>
	            				</td>
	            			</tr>
	            			<?php $i++;}?>
	            			<?php } else {?>
	            				<tr>
	            					<td class="text-center" colspan="5">Sorry. No product found!</td>
	            				</tr>
	            			<?php }?>
	            		</table>
	            			<div class="text-center"><?php echo $paging; ?></div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</section>
</div>

<?php $this->load->view('vfooter');?>