<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fas fa-save"></i> Report</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Report</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="box">
					<div class="box-header with-border">
						<div class="text-center"><strong>Orders</strong></div>
					</div>
					<div class="box-body">
						<form class="form-horizontal" method="POST" action="<?php echo site_url('dashboard') . '/tools'; ?>">
							<div class="form-group">
								<label class="col-xs-3 control-label">From</label>
								<div class="col-xs-7">
								    <div class="input-group date" id="dp1">
								    	<input type="text" name="ostartdate" class="form-control" placeholder="From" value="<?php echo date('M/d/Y'); ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								    </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">To</label>
								<div class="col-xs-7">
								    <div class="input-group date" id="dps1">
								    	<input type="text" class="form-control" name="oenddate" placeholder="To" value="<?php echo date('M/d/Y', strtotime('+30 days', strtotime(str_replace('/', '-', date('d/m/Y'))))) . PHP_EOL; ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								    </div>
								</div>
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="text-center">
											<button type="submit" class="btn btn-success" name="o1" value="o1"><i class="fas fa-file-excel"></i> Excel</button>
											<button type="submit" class="btn btn-danger" name="o2" value="o2"><i class="fas fa-file-pdf"></i> PDF</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<div class="box-header with-border">
						<div class="text-center"><strong>Sales</strong></div>
					</div>
					<div class="box-body">
						<form class="form-horizontal" method="POST" action="<?php echo site_url('dashboard') . '/tools'; ?>">
							<div class="form-group">
								<label class="col-xs-3 control-label">From</label>
								<div class="col-xs-7">
								    <div class="input-group date" id="dp2">
								    	<input type="text" class="form-control" name="sstartdate" placeholder="From" value="<?php echo date('M/d/Y'); ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								    </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">To</label>
								<div class="col-xs-7">
									<div class="input-group date" id="dps2">
									    <input type="text" class="form-control" name="senddate" placeholder="To" value="<?php echo date('M/d/Y', strtotime('+30 days', strtotime(str_replace('/', '-', date('d/m/Y'))))) . PHP_EOL; ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="text-center">
											<button type="submit" class="btn btn-success" name="s1" value="s1"><i class="fas fa-file-excel"></i> Excel</button>
											<button type="submit" class="btn btn-danger" name="s2" value="s2"><i class="fas fa-file-pdf"></i> PDF</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box">
					<div class="box-header with-border">
						<div class="text-center"><strong>Products</strong></div>
					</div>
					<div class="box-body">
						<form class="form-horizontal" method="POST" action="<?php echo site_url('dashboard') . '/tools'; ?>">
							<div class="form-group">
								<label class="col-xs-3 control-label">From</label>
								<div class="col-xs-7">
									<div class="input-group date" id="dp3">
								    	<input type="text" class="form-control" name="pstartdate" placeholder="From" value="<?php echo date('M/d/Y'); ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								    </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label">To</label>
								<div class="col-xs-7">
									<div class="input-group date" id="dps3">
									    <input type="text" class="form-control" name="penddate" placeholder="To" value="<?php echo date('M/d/Y', strtotime('+30 days', strtotime(str_replace('/', '-', date('d/m/Y'))))) . PHP_EOL; ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="text-center">
											<button type="submit" class="btn btn-success" name="p1" value="p1"><i class="fas fa-file-excel"></i> Excel</button>
											<button type="submit" class="btn btn-danger" name="p2" value="p2"><i class="fas fa-file-pdf"></i> PDF</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- <section class="content-header">
		<h1><i class="fas fa-comments"></i> SMS</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">SMS</li>
		</ol>
	</section> -->
	<!-- <section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">
						<div class="btn-group">
							<form method="post" name="form" id="form" action="">
								<input type="hidden" name="o" value="0" />
								<input type="submit" name="enable" class="btn btn-flat btn-primary" value="Auto Tracking SMS: ON" />
							</form>
							ifelse
							<form method="post" name="form" id="form" action="">
								<input type="hidden" name="o" value="1" />
								<input type="submit" name="enable" class="btn btn-flat btn-danger" value="Auto Tracking SMS: OFF" />
							</form>
						</div>
						<div class="btn-group">
							<a href="<?php echo site_url('setting'); ?>#site"><button class="btn btn-flat btn-info"><i class="fa fa-cog"></i></button></a>
						</div>
					</div>
					<div class="box-body">
						<form id='kepada' action=\"\" class=\"form-horizontal\" method=\"POST\">
							<div class="form-group">
								<label class="col-sm-2 control-label">Hantar Kepada</label>
								<div class="col-sm-10">
									<select name="pref" id="kepada" class="bs-select form-control" onchange="this.form.submit();">
										<option value="semua">Semua Pembeli</option>
										<option value="baru">Pembeli Baru</option>
										<option value="lama">Pembeli Lama</option>
										<option value="tel">No. Telefon</option>
									</select>
								</div>
							</div>
						</form>
						<form action="" id="sent" class="form-horizontal" method="POST">
							<div class="form-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Penerima</label>
									<div class="col-sm-10">
										<textarea name="no" class="form-control" value="0000000000" placeholder="Example: 60123498955,60176498455"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Mesej</label>
									<div class="col-sm-10">
										<textarea name="sms" class="form-control" value="000000000" placeholder="Tulis mesej anda disini">RM0.00</textarea>
									</div>
								</div>
								<input type="hidden" name="pref" value="preference" />
								<div class="box-footer">
									<div class="col-sm-offset-2">
										<button type="submit" form="sent" class="btn btn-primary" name="submit">Hantar</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section> -->
</div>

<?php $this->load->view('vfooter');?>