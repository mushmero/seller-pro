<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fas fa-shopping-cart"></i> Latest Orders</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Latest Orders</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
					<a href="<?php echo site_url('dashboard') . '/forms'; ?>"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i> Submit New Order</button></a>
				</div>
			</div>
		</div>
		<div class="clearfix"><br></div>
		<div class="row">
			<div class="col-xs-12">
				<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="table-responsive no-padding">
					<table class="table">
						<tr>
							<td><button type="button" class="btn btn-block btn-success btn-flat"><i class="fas fa-print"></i></button></td>
							<td><button type="button" class="btn btn-block btn-success btn-flat"><i class="fas fa-download"></i></button></td>
							<td><button type="button" class="btn btn-block btn-info btn-flat"><i class="fas fa-truck"></i></button></td>
							<td><button type="button" class="btn btn-block btn-warning btn-flat"><i class="fas fa-hand-holding-usd"></i></button></td>
							<td><button type="button" class="btn btn-block btn-danger btn-flat"><i class="fas fa-times"></i></button></td>
							<td><button type="button" class="btn btn-block btn-info btn-flat"><i class="fas fa-check-circle"></i></button></td>
							<td><button type="button" class="btn btn-block btn-primary btn-flat"><i class="fas fa-check-double"></i></button></td>
						</tr>
						<tr>
							<td>Generate Invoice</td>
							<td>Download Invoice</td>
							<td>Method: Postage</td>
							<td>Method: COD</td>
							<td>Cancel Order</td>
							<td>Re-Accept Order</td>
							<td>Complete Order</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-success">
					<div class="box-header">
						<h3 class="box-title">Orders List</h3>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th class="text-center"> # </th>
								<th width="15%"> Buyer Name </th>
								<th width="45%"> Product </th>
								<th class="text-center"> Bank </th>
								<th class="text-center"> Action </th>
							</tr>
							<?php if (is_array($order)) {?>
								<?php $i = $this->uri->segment(4) + 1;foreach ($order as $o) {?>
								<tr>
									<td class="text-center" ><?php echo $i; ?></td>
									<td> <a href="<?php echo site_url('dashboard'); ?>/orders/vieworder/<?php echo $o->id; ?>"><?php echo $o->name; ?></a> </td>
									<td> <?php echo $o->produk; ?></td>
									<td><b><?php echo $o->bankname; ?></b><br /><?php echo date('d/m/Y', strtotime($o->bankdate)); ?>&nbsp;<?php echo $o->banktime; ?></td>
									<td>
										<?php if ($o->status == 0) {?>
										 	<div class="btn-group">
										 		<form method="post" name="form" action="<?php echo site_url('dashboard') . '/orders/' . $this->uri->segment(3); ?>">
										 			<input type="hidden" name="postage" value="1">
										 			<input type="hidden" name="pid" value="<?php echo $o->id; ?>">
										 			<input type="hidden" name="states" value="<?php echo $o->states_id; ?>">
										 			<button type="submit" name="button" class="btn btn-block btn-info btn-flat" title="Postage"><i class="fas fa-truck"></i></button>
										 		</form>
										 	</div>
										 	<div class="btn-group">
										 		<form method="post" name="form" action="<?php echo site_url('dashboard') . '/orders/' . $this->uri->segment(3); ?>">
										 			<input type="hidden" name="cod" value="2">
										 			<input type="hidden" name="pid" value="<?php echo $o->id; ?>">
										 			<button type="submit" name="button" class="btn btn-block btn-warning btn-flat" title="COD"><i class="fas fa-hand-holding-usd"></i></button>
										 		</form>
										 	</div>
											<?php if ($o->print == 0) {?>
										 	<div class="btn-group">
										 		<form method="POST" action="<?php echo site_url('dashboard') . '/invoice/' . $o->id; ?>" target="_blank" onsubmit="setTimeout(function () { window.location.reload(); }, 15)">
										 			<button type="submit" title="Generate Invoice" class="btn btn-block btn-success btn-flat"><i class="fas fa-print"></i></button>
										 		</form>
										 	</div>
										 	<?php }?>
										<?php } else if ($o->status == 1) {?>
										 	<?php if ($o->print == 1) {?>
												<div class="btn-group">
											 		<a href="<?php echo base_url('/uploads/invoice/' . $o->pdflink); ?>" target="_blank">
											 			<button type="button" title="Download Invoice" class="btn btn-block btn-success btn-flat"><i class="fas fa-download"></i></button>
											 		</a>
											 	</div>
											<?php }?>
											<?php if ($o->iscancel == 0) {?>
		                    				<div class="btn-group">
					                    		<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
					                    			<input type="hidden" name="cancel" value="1" />
					                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
					                    			<button type="submit" class="btn btn-block btn-danger btn-flat" title="Cancel Order"><i class="fas fa-times"></i></button>
					                    		</form>
					                    	</div>
					                    	<?php } else if ($o->iscancel == 1) {?>
					                    	<div class="btn-group">
				                    			<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
				                    				<input type="hidden" name="reaccept" value="0" />
					                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
				                    				<button type="submit" class="btn btn-block btn-info btn-flat" title="Re-accept Order"><i class="fas fa-check-circle"></i></button>
					                    		</form>
					                    	</div>
					                    	<?php }?>
					                    	<div class="btn-group">
										 		<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
				                    				<input type="hidden" name="complete" value="2" />
					                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
				                    				<button type="submit" class="btn btn-block btn-primary btn-flat" title="Complete Order"><i class="fas fa-check-double"></i></button>
					                    		</form>
										 	</div>
										<?php } else if ($o->status == 2) {?>
											<div class="label label-warning text-center">Orders Completed</div>
										<?php }?>
									</td>
								</tr>
								<?php $i++;}?>
							<?php } else {?>
								<tr>
									<td colspan="5">No orders found.</td>
								</tr>
							<?php }?>
						</table>
					</div>
				</div>
				<div class="text-center">
					<?php echo $paging; ?>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>