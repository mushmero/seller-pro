<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-fw fa-edit"></i> Order Form</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Order Form</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<?php echo (isset($insert_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$insert_success</strong></div>" : ''; ?>
  						<?php echo (isset($insert_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$insert_error</strong></div>" : ''; ?>
  						<?php echo (isset($upload_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$upload_error[error]</strong></div>" : ''; ?>
					</div>
					<form action="<?php echo site_url('dashboard'); ?>/forms" id="borang" name="borang" method="POST" ENCTYPE="multipart/form-data">
						<div class="box-body">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Customer Name</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Phone</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Email</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" name="name" data-required="1" class="form-control" value="<?php echo set_value('name'); ?>" placeholder="Eg: John Doe">
        								<?php echo form_error('name', '<p class="help-inline">', '</p>'); ?>
        							</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="tel" name="phone" class="form-control" value="<?php echo set_value('phone'); ?>" placeholder="Eg: 0123456789">
        								<?php echo form_error('phone', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="email" name="email" class="form-control" value="<?php echo set_value('email'); ?>" placeholder="Eg: john@doe.com">
        								<?php echo form_error('email', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Address</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" name="add1" class="form-control" placeholder="Address 1" value="<?php echo set_value('add1'); ?>">
        								<?php echo form_error('add1', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" name="add2" class="form-control" placeholder="Address 2" value="<?php echo set_value('add2'); ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">Postcode</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">City</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label class="control-label">State</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" name="postcode" class="form-control" placeholder="Postcode" value="<?php echo set_value('postcode'); ?>">
										<?php echo form_error('postcode', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" name="cities" class="form-control" placeholder="City" value="<?php echo set_value('cities'); ?>">
										<?php echo form_error('cities', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<select class="bs-select form-control" name="states">
										<?php if (is_array($states)) {?>
										<option value="" selected disabled>State</option>
										<?php foreach ($states as $s) {?>
										<option value="<?php echo $s->state_id; ?>" <?php if ($s->state_id == set_value('states')) {echo "selected";}?> ><?php echo $s->name; ?></option>
										<?php }}?>
										</select>
										<?php echo form_error('states', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Product</label>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Quantity</label>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label">Unit Price (RM)</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<select name="product" class="bs-select form-control">
										<?php if (is_array($products)) {?>
											<option value="" selected disabled>Please Choose</option>
										<?php foreach ($products as $p) {?>
											<option value="<?php echo $p->id; ?>" <?php if ($p->id == set_value('product')) {echo 'selected';}?>><?php echo $p->produk; ?> (<?php echo $p->unit; ?> unit)</option>
										<?php }?>
										<?php } else {?>
											<option value="0">No Product</option>
										<?php }?>
										</select>
	        							<?php echo form_error('product', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="number" name="unit" value="<?php echo set_value('unit'); ?>" class="form-control" />
        								<?php echo form_error('unit', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="number" name="price" class="form-control" value="<?php echo set_value('price') ?>" placeholder="Price" step=".01">
        								<?php echo form_error('price', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
							</div>
		              		<div class="row">
		              			<div class="col-sm-6">
		              				<div class="form-group">
		              					<label class="control-label">Payment Method</label>
		              				</div>
		              			</div>
		              			<div class="col-sm-3">
		              				<div class="form-group">
		              					<label class="control-label">Date</label>
		              				</div>
		              			</div>
		              			<div class="col-sm-3">
		              				<div class="form-group">
		              					<label class="control-label">Time</label>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="row">
		              			<div class="col-sm-6">
		              				<div class="form-group">
		              					<select name="bank" class="bs-select form-control">
										<?php if (is_array($banks)) {?>
		              					<option value="" selected disabled>Please Choose</option>
		              					<option value="cod" <?php if ("cod" == set_value('bank')) {echo "selected";}?> > Cash On Delivery (COD)</option>
		              					<?php foreach ($banks as $b) {?>
		              						<option value="<?php echo $b->id ?>" <?php if ($b->id == set_value('bank')) {echo "selected";}?> ><?php echo $b->bankname; ?></option>
		              					<?php }?>
		              					<?php }?>
			              				</select>
	        								<?php echo form_error('bank', '<p class="help-inline">', '</p>'); ?>
		              				</div>
		              			</div>
		              			<div class="col-sm-3">
		              				<div class="form-group">
		              					<div class="input-group date" id="dp1">
									    	<input type="text" class="form-control" name="date" placeholder="Date" value="<?php echo set_value('date'); ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										</div>
        									<?php echo form_error('date', '<p class="help-inline">', '</p>'); ?>
		              				</div>
		              			</div>
		              			<div class="col-sm-3">
		              				<div class="form-group">
		              					<div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
			              					<input type="text" name="times" class="form-control" value="<?php echo set_value('times'); ?>" placeholder="Time">
			              					<span class="input-group-addon">
			              						<span class="glyphicon glyphicon-time"></span>
			              					</span>
		              					</div>
        								<?php echo form_error('times', '<p class="help-inline">', '</p>'); ?>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="row">
		              			<div class="col-sm-8">
		              				<div class="form-group">
		              					<label class="control-label">Additional Note</label>
		              				</div>
		              			</div>
		              			<div class="col-sm-4">
		              				<div class="form-group">
		              					<label class="control-label">Payment Receipt (.jpg, .gif, .png & .pdf) <small> Max 5MB</small></label>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="row">
		              			<div class="col-sm-8">
		              				<div class="form-group">
		              					<textarea type="text" maxlength="200" name="notes" class="form-control" placeholder="Eg: Blue colour"><?php echo set_value('notes'); ?></textarea>
		              				</div>
		              			</div>
		              			<div class="col-sm-2">
		              				<div class="form-group">
		              					<input type="file" name="receipt" accept="image/gif, image/jpeg, application/pdf" value="<?php echo set_value('receipt'); ?>">
        								<?php echo form_error('receipt', '<p class="help-inline">', '</p>'); ?>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="row">
		              			<div class="col-sm-4">
		              				<div class="form-group">
		              					<button type="submit" form="borang" class="btn btn-info" name="addorder">Send!</button>
		              				</div>
		              			</div>
		              		</div>
		              	</div>
		            </form>
		        </div>
		    </div>
		</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>