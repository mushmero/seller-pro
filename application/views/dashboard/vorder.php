<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-eye"></i> View Order</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">View Order</li>
		</ol>
	</section>
	<?php foreach ($orders as $o) {
	?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
	            <div class="col-md-6">
	              	<div class="nav-tabs-custom">
	                	<ul class="nav nav-tabs pull-right">
	                  		<li class="dropdown">
	                    		<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fas fa-wrench"></i> Options <span class="caret"></span></a>
	                    			<ul class="dropdown-menu">
	                    				<li role="presentation">
				                    		<?php if ($o->print == 1) {?>
				                    		<a href="<?php echo base_url('/uploads/invoice/' . $o->pdflink); ?>" class="text-center"><button class="btn btn-link"> Download Invoice </button></a>
				                    		<?php } else {?>
				                    		<a href="<?php echo site_url('dashboard'); ?>/invoice/<?php echo $o->id; ?>" class="text-center" target="_blank"><button class="btn btn-link"> Generate Invoice </button></a>
				                    		<?php }?>
				                    	</li>
				                    	<?php if ($o->status != 2) {?>
	                    				<li role="presentation">
	                    					<a href="<?php echo site_url('dashboard') . '/orders/editorder/' . $o->id; ?>" class="text-center"><button class="btn btn-link"> Edit Order </button></a>
	                    				</li>
				                    	<li role="presentation">
	                    					<?php if ($o->iscancel == 0) {?>
				                    		<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
				                    			<input type="hidden" name="cancel" value="1" />
				                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
				                    			<button type="submit" class="btn btn-danger"> Cancel Order </button>
				                    		</form>
				                    		<?php } else if ($o->iscancel == 1) {?>
			                    			<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
			                    				<input type="hidden" name="reaccept" value="0" />
				                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
			                    				<button type="submit" class="btn btn-info"> Re-accept Order </button>
				                    		</form>
				                    		<?php }?>
				                    	</li>
					                    <?php } else if ($o->status == 2) {?>
					                    	<form method="POST" class="text-center" action="<?php echo site_url('dashboard') . '/orders/vieworder/' . $o->id; ?>" >
				                    				<input type="hidden" name="complete" value="2" />
					                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
				                    				<button type="submit" class="btn btn-success"> Complete Order </button>
					                    		</form>
					                    <?php }?>
				                    </ul>
				            </li>
				            <li class="pull-left header"><i class="fas fa-shopping-basket"></i> Order Information</li>
				        </ul>
	            		<div class="tab-content">
	            			<div class="tab-pane active">
	            				<div class="row">
	            					<div class="col-md-5"><strong>Order No</strong></div>
	            					<div class="col-md-7">#<?php echo $o->id; ?></div>
	            				</div>
		            		<div class="row">
		            			<div class="col-md-5"><strong>Date & Time</strong></div>
		            			<div class="col-md-7"><?php echo date('d/m/Y', strtotime($o->date)) . ' | ' . date('h:m A', $o->time); ?> </div>
		            		</div>
		            		<div class="row">
		            			<div class="col-md-5"><strong>Status</strong></div>
		            			<div class="col-md-7">
		            				<?php if ($o->iscancel == 1) {?>
		            				<span class="label label-danger"> Canceled </span>
		            				<?php }?>
		            				<?php if ($o->status == 0 && $o->iscancel != 1) {?>
		            				<span class="label label-warning"> Fresh order. Please generate invoice first! </span>
		            				<?php }?>
		            				<?php if ($o->status == 1 && $o->iscancel != 1) {?>
		            				<span class="label label-info"> In process </span>
		            				<?php }?>
		            				<?php if ($o->status == 2 && $o->iscancel != 1) {?>
		            				<span class="label label-info"> In shipping </span>&nbsp;
		            				<button onclick="linkTrack('<?php echo $o->trackingno; ?>')" class="btn btn-warning"><i class="fas fa-search"></i> Track Parcel</button>
		            				<?php }?>
		            				<?php if ($o->status == 3 && $o->iscancel != 1) {?>
		            				<span class="label label-success"> Completed </span>
		            				<?php }?>
		            			</div>
		            		</div>
		            		<div class="row">
		            			<div class="col-md-5"><strong>Amount Paid</strong></div>
		            			<div class="col-md-7"> RM<?php echo sprintf('%0.2f', $o->price * $o->unit); ?></div>
		            		</div>
	            				<div class="row">
	            					<div class="col-md-5"><strong>Payment Method</strong></div>
	            					<div class="col-md-7"><?php echo $o->bankname; ?>  <a href="#" data-toggle="modal" data-target="#receipt" class="btn btn-link"><span class="label label-primary"><i class="far fa-file-pdf"></i> Payment Details</span></a>
	            					</div>
	            					<div id="receipt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ReceiptLabel" aria-hidden="true">
		            					<div class="modal-dialog">
		            						<div class="modal-content">
		            							<div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											        <h4 class="modal-title" id="myModalLabel">Proof of Payment</h4>
											     </div>
		            							<div class="modal-body">
		            								<div style="text-align: center;">
														<iframe src="<?php echo base_url() . 'uploads/resit/' . $o->resit; ?>"
														style="width:500px; height:500px;" frameborder="0"></iframe>
													</div>
												</div>
										      	<div class="modal-footer">
										        	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		            							</div>
		            						</div>
		            					</div>
	            					</div>
	            				</div>
	            			</div>
	            		</div>
	        		</div>
	    		</div>
	    		<div class="col-md-6">
	        		<div class="nav-tabs-custom">
	            		<ul class="nav nav-tabs pull-right">
	                		<li class="pull-left header"><i class="far fa-address-card"></i> Customer Information</li>
	            		</ul>
	            		<div class="tab-content">
	                		<div class="tab-pane active">
	                 			<div class="row">
	                 				<div class="col-md-5"><strong>Customer Name</strong></div>
	                 				<div class="col-md-7"><?php echo $o->name; ?></div>
	                 			</div>
	                 			<div class="row">
	                 				<div class="col-md-5"><strong>Phone Number</strong></div>
	                 				<div class="col-md-7"> <?php echo $o->phone; ?> </div>
	                 			</div>
		                 	<div class="row">
		                 		<div class="col-md-5"><strong>Email</strong></div>
		                 		<div class="col-md-7"> <?php if ($o->email != '') {echo $o->email;} else {?><span class="label label-danger"> Tiada </span><?php }?> </div>
		                 	</div>
		                 	<div class="row">
		                 		<div class="col-md-5"><strong>Shipping Address</strong></div>
		                 		<div class="col-md-7"> <?php echo $shipping_address; ?> </div>
		                 	</div>
		                 	<div class="row">
		                 		<div class="col-md-5"><strong>Additional Information</strong></div>
		                 		<div class="col-md-7"> <?php if ($o->info != '') {echo $o->info;} else {?><span class="label label-danger"> N/A </span><?php }?> </div>
		                 	</div>
		                </div>
		            </div>
		        </div>
		    </div>
	 		</div>
	 		<div class="row">
	    		<div class="col-md-6">
	        		<div class="nav-tabs-custom">
		            <ul class="nav nav-tabs pull-right">
		                 <li class="pull-left header"><i class="fa fa-cubes"></i>Item Bought</li>
		            </ul>
	                	<div class="tab-content">
	                  		<div class="tab-pane active">
	                  			<table class="table table-condensed">
	                  			<tr>
	                  				<th class="text-center"> # </th>
	                  				<th> Product </th>
	                  				<th class="text-center"> Quantity </th>
	                  			</tr>
	                  			<tr>
	                  				<td class="text-center"> 1 </td>
	                  				<td><b><?php echo $o->produk; ?></b></td>
	                  				<td class="text-center"> <?php echo $o->unit; ?> Units </td>
	                  			</tr>
	                  			</table>
	                  		</div>
	                  	</div>
	              	</div>
	          	</div>
	          	<div class="col-md-6">
	              	<div class="nav-tabs-custom">
	              		<ul class="nav nav-tabs pull-right">
	                		<li class="pull-left header"><i class="far fa-money-bill-alt"></i> Payment Details</li>
	            		</ul>
	                	<div class="tab-content color-palette">
	                  		<div class="tab-pane active">
		                  	<div class="row">
		                  		<div class="col-md-4"><strong> Amount </strong></div>
		                  		<div class="col-md-6"> RM<?php echo sprintf('%0.2f', (($o->price * $o->unit) - $o->postprice)); ?> </div>
		                  	</div>
		                  	<div class="row">
		                  		<div class="col-md-4"><strong> Postage </strong></div>
		                  		<div class="col-md-6"> RM<?php echo sprintf('%0.2f', $o->postprice) ?> <span class="label label-success"><?php if (isset($reason)) {echo $reason;}?></span></div>
		                  	</div>
		                  	<div class="row">
		                  		<div class="col-md-4"><strong> GST (<?php echo $gstrate; ?>%) </strong></div>
		                  		<div class="col-md-6"> RM<?php echo sprintf('%0.2f', $gst); ?></div>
		                  	</div>
		                  	<div class="row">
		                  		<div class="col-md-4"><strong> Total </strong></div>
		                  		<div class="col-md-6"> <b>RM<?php echo sprintf('%0.2f', (($o->price * $o->unit) + $gst)); ?></b></div>
		                  	</div>
	                  		</div>
	              		</div>
	          		</div>
	      		</div>
	  		</div>
	  		<div class="row">
	  			<div class="col-xs-12">
	  				<h1 class="text-center"><a href="<?php echo site_url('dashboard'); ?>/orders"><i class="fa fa-arrow-circle-left"></i> Back</a></h1>
	  			</div>
	  		</div>
	</section>
<?php }?>
	<!-- end view order -->
</div>

<?php $this->load->view('vfooter');?>