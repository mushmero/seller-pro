<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Customer Invoice">
    <meta name="author" content="mushmero">

    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <style>
      .invoice-head td {
        padding: 0 8px;
      }
      .container {
      	padding-top:20px;
      }
      .invoice-body{
      	background-color:transparent;
      }
      .invoice-thank{
      	margin-top: 20px;
        margin-bottom: 10px;
      	padding: 5px;
      }
      address{
      	margin-top:15px;
      }
      .table > tbody > tr > .no-line {
        border-top: none;
      }
      .thin-line {
        border-top: 1px solid;
      }
      .thick-line {
        border-bottom: 2px solid;
      }
      .invoice-bottom{
        padding: 10px;
      }
      .table > tbody > tr > .subline {
        padding-bottom: 30px;
      }
    </style>
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="icon" type="image/x-icon" href="<?php echo WEBLOGO; ?>">
  </head>

  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-6">
    			<img src="<?php echo PDFLOGO; ?>" width="60" height="35">
    			<address>
            <strong><?php echo $user['name']; ?> (GST No <?php echo $user['gst']; ?>) </strong>
            <br>
			        <?php echo $user['address']; ?><br>
              <?php echo $user['poskod'] . ' ' . $user['town']; ?><br>
              <?php echo $user['state']; ?><br>
              <span class="fas fa-phone"></span> <?php echo $user['phone']; ?><br>
              <span class="fas fa-globe"></span> <?php echo $user['email']; ?>
		    	</address>
    		</div>
    		<div class="col-xs-6" align="right">
    			<table class="invoice-head">
    				<tbody>
              <tr>
                <td colspan="2" class="pull-left"><h4><strong>Billed to</strong></h4></td>
              </tr>
    					<tr>
    						<td class="pull-left"><strong>Customer #</strong></td>
    						<td><?php echo $order[0]->name; ?></td>
    					</tr>
    					<tr>
    						<td class="pull-left"><strong>Invoice #</strong></td>
    						<td><?php echo $invoice_id; ?></td>
    					</tr>
    					<tr>
    						<td class="pull-left"><strong>Date</strong></td>
    						<td><?php echo $order[0]->bankdate; ?></td>
    					</tr>
    					<tr>
    						<td class="pull-left"><strong>Phone</strong></td>
    						<td><?php echo $order[0]->phone; ?></td>
    					</tr>
    				</tbody>
    			</table>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-6">
    			<h2>Invoice</h2>
    		</div>
    	</div>
    	<div class="row">
		  	<div class="col-xs-12 well invoice-body">
		  		<table class="table table-condensed" width="100%">
					<thead>
						<tr>
              <th>#</th>
							<th width="70%">Description</th>
							<th>Quantity</th>
							<th>Unit Price (RM)</th>
              <th>Subtotal (RM)</th>
						</tr>
					</thead>
					<tbody>
          <?php $i = 1;foreach ($order as $o) {?>
            <tr>
              <td class="thick-line subline">
                <?php echo $i; ?>
              </td>
              <td class="thick-line subline">
                <?php echo $o->produk ?>
              </td>
              <td class="thick-line subline">
                <?php echo $o->unit; ?>
              </td>
              <td class="thick-line subline">
                <?php echo $perunit; ?>
              </td>
              <td class="thick-line subline">
                <?php echo $subtotal; ?>
              </td>
            </tr>
          <?php $i++;}?>
            <tr>
							<td colspan="2">&nbsp;</td>
							<td colspan="2"><strong>Total</strong></td>
							<td><strong>RM <?php echo $subtotal; ?></strong></td>
						</tr>
            <tr>
              <td class="no-line" colspan="2">&nbsp;</td>
              <td class="no-line" colspan="2"><strong>Shipping</strong></td>
              <td class="no-line"><strong>RM <?php echo $shipping; ?></strong></td>
            </tr>
            <tr>
              <td class="no-line" colspan="2">&nbsp;</td>
              <td class="no-line" colspan="2"><strong>GST (<?php echo $user['gstrate']; ?>%)</strong></td>
              <td class="no-line"><strong>RM <?php echo $gst; ?></strong></td>
            </tr>
            <tr>
              <td class="no-line" colspan="2">&nbsp;</td>
              <td class="no-line" colspan="2"><strong>Grand Total</strong></td>
              <td class="no-line"><strong>RM <?php echo $gtotal; ?></strong></td>
            </tr>
					</tbody>
				</table>
		  	</div>
  		</div>
      <div class="row">
        <div class="cl-xs-6">
          <address>
            <strong>Payment Method:</strong>
              <?php echo $order[0]->bankname; ?>
          </address>
        </div>
      </div>
  		<div class="row">
  			<div class="col-xs-12 invoice-thank">
  				<h5 style="text-align:center;"><strong><i>Thank you for choosing us as your reliable seller.</i></strong></h5>
  			</div>
  		</div>
    </div>

    <script src="<?php echo base_url('/assets/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
  </body>
</html>