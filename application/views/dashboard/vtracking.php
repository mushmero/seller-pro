<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fas fa-truck-loading"></i> Tracking Info</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Tracking Info</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover" width="100%">
							<tr>
								<th class="text-center">#</th>
								<th width="15%">Customer</th>
								<th width="50%">Products</th>
								<th>Tracking No</th>
							</tr>
							<?php if (is_array($order)) {?>
							<?php $i = $this->uri->segment(4) + 1;foreach ($order as $o) {?>
							<tr>
								<td class="text-center"><?php echo $i; ?></td>
								<td><?php echo $o->name; ?></td>
								<td><?php echo $o->produk; ?></td>
								<td>
									<?php if ($o->postage != 3) {?>
									<?php if ($o->trackingno == '') {?>
									<form action="<?php echo site_url('dashboard') . '/tracking'; ?>" method="POST" class="form-inline">
										<input type="text" name="trackingno" class="form-control" placeholder="eg:FQ001791875MY">
										<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
	                    				<input type="hidden" name="trackstatus" value="2" >
										<button type="submit" class="btn btn-flat btn-success" title="Submit Tracking No"><i class="fas fa-user-check"></i></button>
									</form>
									<?php } else {?>
										<div class="btn-group">
			                    			<form method="POST" action="<?php echo site_url('dashboard') . '/tracking'; ?>">
			                    				<input type="hidden" name="edit" value="1" >
				                    			<input type="hidden" name="oid" value="<?php echo $o->id; ?>">
			                    				<button type="submit" class="btn btn-block btn-info btn-flat" title="Edit Tracking No"><i class="fas fa-edit"></i></button>
				                    		</form>
			                    		</div>
										<?php echo $o->trackingno; ?>
		                    		<?php }?>
		                    		<?php } else {?>
		                    			<div class="text-center btn-group"><button type="button" class="btn btn-block btn-warning btn-flat"><i class="fas fa-hand-holding-usd"></i> COD</button></div>
		                    		<?php }?>
								</td>
							</tr>
							<?php $i++;}?>
							<?php } else {?>
							<tr>
								<td colspan="4">No orders found.</td>
							</tr>
							<?php }?>
						</table>
					</div>
				</div>
				<div class="text-center"><?php echo $paging; ?></div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>