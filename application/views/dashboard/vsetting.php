<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
    	<h1><i class="fas fa-cogs"></i> Setting </h1>
    	<ol class="breadcrumb">
    		<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
        	<li class="active">Setting</li>
    	</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
				<?php echo (isset($upload_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$upload_error[error]</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
	        <div class="col-md-3">
	          <!-- Profile Image -->
	          <div class="box box-danger">
	            <div class="box-body box-profile">
	              <img class="profile-user-img img-responsive img-circle" src="<?php if (is_array($user['picture'])) {echo base_url('/uploads/profile/') . $user['picture'];} else {echo base_url('/assets/dist/img/avatar04.png');}?>" alt="User profile picture">

	              <h3 class="profile-username text-center"><?php echo $user['username']; ?></h3>

	              <p class="text-muted text-center"><?php echo $user['levelName']; ?></p>

	              <ul class="list-group list-group-unbordered">
	                <li class="list-group-item">
	                  <b>Last Login</b> <a class="pull-right"><?php echo date('h:i A', $user['lastlogin']); ?></a>
	                </li>
	                <li class="list-group-item">
	                  <b>Last IP</b> <a class="pull-right"><?php echo $user['ip']; ?></a>
	                </li>
	                <li class="list-group-item">
	                  <b>Company Reg. No</b> <a class="pull-right"><?php if ($user['company'] != '') {echo $user['company'];} else {echo "N/A";}?></a>
	                </li>
	              </ul>
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- <div class="box box-success">
	          	<div class="box-header with-border">
	          		<h3 class="box-title"><i class="fas fa-comments"></i> SMS</h3>
	          	</div>
	          	<div class="box-body">
	          		<form action="<?php echo site_url('dashboard/setting') ?>" method="POST">
	          			           		<div class="form-group">
	          			           			<label class="control-label">SMS Username</label>
	          			           			<input type="text" name="smsname" value="<?php echo $user['smsname']; ?>" class="form-control" placeholder="username" />
	          			           		</div>
	          			           		<div class="form-group">
	          			           			<label class="control-label">SMS Password</label>
	          			           			<input type="password" name="smspass" value="<?php echo $user['smspass']; ?>" class="form-control" placeholder="xxxxxx" />
	          			           		</div>
	          			           		<div class="margin-top-10">
	          			           			<button type="submit" name="smsbtn" value="1" class="btn btn-success"> Set </button>
	          			           		</div>
	          			           	</form>
	          	</div>
	          </div> -->
	          <!-- /.box -->
	        </div>
	    	<div class="col-md-9">
			    <div class="box box-warning">
			    	<div class="box-header with-border">
			    		<h3 class="box-title">Store Name</h3>
			    	</div>
			    	<div class="box-body">
			    		<form action="" method="POST">
			    			<div class="form-group">
			    				<label class="control-label">Store Name</label>
			    				<input type="text" name="storename" class="form-control" placeholder="Eg: My Store" value="<?php echo $user['name']; ?>">
			    			</div>
			           		<div class="margin-top-10">
			           			<button type="submit" name="storebtn" value="1" class="btn btn-warning"> Change </button>
			           		</div>
			    		</form>
			    	</div>
			    </div>
				<div class="nav-tabs-custom">
			        <ul class="nav nav-tabs">
			          	<li class="active"><a href="<?php echo site_url('dashboard/setting#account'); ?>" data-toggle="tab">Account</a></li>
			          	<li><a href="<?php echo site_url('dashboard/setting#password'); ?>" data-toggle="tab">Password</a></li>
			          	<li><a href="<?php echo site_url('dashboard/setting#system'); ?>" data-toggle="tab">System</a></li>
			        </ul>
			        <div class="tab-content">
			           <div class="tab-pane active" id="account">
			               <form action="<?php echo site_url('dashboard/setting') ?>" method="POST" id="accounts">
			                   <div class="form-group">
			                   	<label class="control-label">Username</label>
			                   	<input type="text" name="username" value="<?php echo $user['username']; ?>" placeholder="Eg: John Doe" class="form-control" />
			                   </div>
			                   <div class="form-group">
			                   	<div class="row">
			                   		<div class="col-xs-6">
					                   	<label class="control-label">Company Reg. No</label>
					                   	<input type="text" name="companyNo" class="form-control" value="<?php echo $user['company']; ?>" placeholder="Eg: xxx-123xx">
				                   </div>
				                   <div class="col-xs-6">
					                   	<label class="control-label">Tax No</label>
					                   	<input type="text" name="gstNo" class="form-control" value="<?php echo $user['gst']; ?>" placeholder="Eg: xxxx-234">
				                   </div>
			                   	</div>
			                   </div>
			                   <div class="form-group">
			                   	<div class="row">
			                   		<div class="col-xs-5">
					                   	<label class="control-label">Email</label>
					                   	<input type="email" name="email" class="form-control" value="<?php echo $user['email']; ?>" placeholder="Eg: order@gmail.com">
			                   		</div>
			                   		<div class="col-xs-4">
					                   	<label class="control-label">Phone No</label>
					                   	<input type="tel" name="phone" class="form-control" value="<?php echo $user['phone']; ?>" placeholder="Eg: 0123456789">
					                </div>
					                <div class="col-xs-3">
					                	<label class="control-label">Tax Rate</label>
					                	<input type="number" name="gstrate" class="form-control" value="<?php echo $user['gstrate']; ?>" placeholder="6">
					                </div>
			                   	</div>
			                   </div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-12">
											<label class="control-label">Address</label>
											<input type="text" name="add1" class="form-control" placeholder="Address" value="<?php echo $user['address']; ?>">
	        								<?php echo form_error('add1', '<p class="help-inline">', '</p>'); ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-4">
											<label class="control-label">Postcode</label>
											<input type="text" name="postcode" class="form-control" placeholder="Poskod" value="<?php echo $user['poskod']; ?>">
											<?php echo form_error('postcode', '<p class="help-inline">', '</p>'); ?>
										</div>
										<div class="col-xs-4">
											<label class="control-label">City</label>
											<input type="text" name="cities" class="form-control" placeholder="Bandar" value="<?php echo $user['town']; ?>">
											<?php echo form_error('cities', '<p class="help-inline">', '</p>'); ?>
										</div>
										<div class="col-xs-4">
			                   				<label class="control-label">States</label>
			                   				<select class="bs-select form-control" name="states">
												<?php if (is_array($states)) {?>
												<option value="" selected disabled>Negeri</option>
												<?php foreach ($states as $s) {?>
												<option value="<?php echo $s->state_id; ?>" <?php if ($s->state_id == $user['state']) {echo "selected";}?> ><?php echo $s->name; ?></option>
											<?php }}?>
											</select>
										</div>
									</div>
								</div>
			               	<div class="margiv-top-10">
			                       <button type="submit" name="detailsbtn" value="1" class="btn btn-primary"> Update </button>
			                   </div>
			               </form>
			           </div>
			           <div class="tab-pane" id="system">
			           	<form action="<?php echo site_url('dashboard/setting') ?>" method="POST" ENCTYPE="multipart/form-data">
			           		<div class="form-group">
			           			<img src="<?php if (is_array($user['picture'])) {echo base_url('/uploads/profile/') . $user['picture'];} else {echo base_url('/assets/dist/img/avatar04.png');}?>" width="200px" /><br /><br />
			           			<input type="file" name="photo" accept="image/png, image/jpeg"><br />
			           			<p>Only PNG/JPEG/JPG accepted. Maximum Size: 5MB. </p><br />
			           			<button type="submit" name="photobtn" value="1" class="btn btn-primary"> Upload </button>
			           		</div>
			           	</form>
			           </div>
			           <div class="tab-pane" id="password">
			           	<form action="<?php echo site_url('dashboard/setting') ?>" method="POST">
			           		<div class="form-group">
			           			<label class="control-label">Current Password</label>
			           			<input type="password" name="current" class="form-control" />
			           			<?php echo form_error('current', '<p class="help-inline">', '</p>'); ?>
			           		</div>
			           		<div class="form-group">
			           			<label class="control-label">New Password</label>
			           			<input type="password" name="new" class="form-control" />
			           			<?php echo form_error('new', '<p class="help-inline">', '</p>'); ?>
			           		</div>
			           		<div class="form-group">
			           			<label class="control-label">Repeat New Password</label>
			           			<input type="password" name="cnew" class="form-control" />
			           			<?php echo form_error('cnew', '<p class="help-inline">', '</p>'); ?>
			           		</div>
			           		<div class="margin-top-10">
			           			<button  type="submit" name="passbtn" class="btn btn-primary"> Change Password </button>
			           		</div>
			           	</form>
			           </div>
			       </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>