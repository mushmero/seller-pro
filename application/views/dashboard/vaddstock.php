<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-plus"></i>Stock Information</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Stock Information</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php echo (isset($update_success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_success</strong></div>" : ''; ?>
  				<?php echo (isset($update_error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
	        			<h3 class="box-title">Stock Availability</h3>
					</div>
					<div class="box-body">
						<form action="<?php echo site_url('dashboard'); ?>/products/addstock" id="addform" class="form-horizontal" method="POST" name="addform">
							<div class="form-group">
								<label class="col-sm-2 control-label">Product</label>
								<div class="col-sm-10">
									<select name="produk" class="bs-select form-control">
										<?php if (is_array($lists)) {?>
										<option value="">Please Choose</option>
										<?php foreach ($lists as $l) {?>
										<option value="<?php echo $l->id; ?>" <?php if ($l->id == set_value('produk')) {echo "selected";}?> ><?php echo $l->produk; ?></option>
										<?php }?>
										<?php } else {?>
										<option value="0">No product</option>
										<?php }?>
									</select>
									<?php echo form_error('produk', '<p class="help-inline">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Quantity (unit)</label>
								<div class="col-sm-10">
									<input type="number" name="unit" class="form-control" value="<?php echo set_value('unit'); ?>" placeholder="Quantity">
									<?php echo form_error('unit', '<p class="help-inline">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Action</label>
								<div class="col-sm-10">
									<select name="type" class="bs-select form-control">
										<option value="">Please Choose</option>
										<option value="1">Add</option>
										<option value="2">Substract</option>
										<option value="3">Change</option>
									</select>
									<?php echo form_error('type', '<p class="help-inline">', '</p>'); ?>
								</div>
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" form="addform" class="btn btn-primary" name="updates">Update</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
	    </div>
	    <div class="row">
	    	<div class="col-xs-12">
	    		<h1 class="text-center"><a href="<?php echo site_url('dashboard'); ?>/products"><i class="fas fa-arrow-circle-left"></i> Back</a></h1>
	    	</div>
	    </div>
	</section>
</div>

<?php $this->load->view('vfooter');?>