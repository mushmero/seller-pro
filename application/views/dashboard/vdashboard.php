<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
  <section class="content-header">
  	<h1><i class="fa fa-tachometer-alt"></i> Dashboard </h1>
  	<ol class="breadcrumb">
  		<li><a href="#"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
  		<li class="active">Home</li>
  	</ol>
  </section>
  <section class="content">
  	<div class="row">
  		<div class="col-lg-3 col-sm-6 col-xs-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fas fa-file-alt"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">New Orders</span>
              <span class="info-box-number"><?php if (!empty($new)) {echo $new[0]->count;} else {echo 0;}?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  		</div>
  		<div class="col-lg-3 col-sm-6 col-xs-6">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fas fa-calendar-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">This Month</span>
              <span class="info-box-number"><?php if (!empty($month)) {echo $month[0]->count;} else {echo 0;}?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  		</div>
      <div class="clearfix visible-sm-block"></div>
  		<div class="col-lg-3 col-sm-6 col-xs-6">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fas fa-shopping-basket"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Orders</span>
              <span class="info-box-number"><?php if (!empty($total)) {echo $total[0]->count;} else {echo 0;}?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  		</div>
  		<div class="col-lg-3 col-sm-6 col-xs-6">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fas fa-user-times"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Reject Orders</span>
              <span class="info-box-number"><?php if (!empty($rejected)) {echo $rejected[0]->count;} else {echo 0;}?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  		</div>
  	</div>
    <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Sales Recap</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!-- <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div> -->
                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-4 text-center">&nbsp;</div>
                    <div class="col-md-4 text-center">
                      <select class="bs-select form-control" name="months" onchange="sendData(this)">
                        <?php if (is_array($monthview)) {?>
                          <option value="all" selected>All</option>
                          <?php foreach ($monthview as $mv) {?>
                            <option value="<?php echo date('m', strtotime($mv->date)); ?>" <?php if ($mv->date == set_value('months')) {echo "selected";}?>><?php echo date('F Y', strtotime($mv->date)); ?></option>
                          <?php }?>
                        <?php } else {?>
                          <option value="none">No Data</option>
                        <?php }?>
                      </select>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                  </div>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="lineChart" height="180"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Goal Completion</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Unit Sold</span>
                    <span class="progress-number"><b><?php echo $soldunit; ?></b>/<?php echo $totalunit; ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: <?php echo $upercent . "%"; ?>"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Products Availibility</span>
                    <span class="progress-number"><b><?php echo $pavail; ?></b>/<?php echo $ptotal; ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width:<?php echo $ppercent . "%"; ?>"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Orders Delivered</span>
                    <span class="progress-number"><b><?php echo $posted->posted; ?></b>/<?php echo $accepted->accept ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: <?php echo $postpercent . "%"; ?>"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Expected Sales</span>
                    <span class="progress-number"><b>RM<?php echo $current; ?></b>/RM<?php echo $expected; ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: <?php echo $exppercent . "%"; ?>"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-aqua">RM<?php if (!empty($revenue)) {echo $revenue;} else {echo sprintf('%02.f', 0);}?></h5>
                    <span class="description-text">TOTAL REVENUE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-red">RM<?php if (!empty($cost)) {echo $cost;} else {echo sprintf('%02.f', 0);}?></h5>
                    <span class="description-text">TOTAL COST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-green">RM<?php if (!empty($profit)) {echo $profit;} else {echo sprintf('%02.f', 0);}?></h5>
                    <span class="description-text">TOTAL PROFIT</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <?php if ($efficiency >= 75) {
	$color = "text-red";
} else if ($efficiency >= 51) {
	$color = "text-yellow";
} else if ($efficiency >= 11) {
	$color = "text-green";
} else if ($efficiency <= 10) {
	$color = "text-aqua";
}?>
                    <h5 class="description-header <?php echo $color; ?>"><?php echo $efficiency . "%"; ?></h5>
                    <span class="description-text">Efficiency Ratio</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
  </section>
</div>

<?php $this->load->view('vfooter');?>