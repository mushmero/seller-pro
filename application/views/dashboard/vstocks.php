<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-list"></i> LogStock</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">LogStock</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
	              <div class="box">
	                <div class="box-header with-border">
	                	<form action="#" method="POST" name="listdown">
	                		<select name="produkn" class="bs-select form-control input-small" data-style="red" onchange="document.listdown.submit();">
	                			<?php if (is_array($lists)) {?>
	                			<option value="">All</option>
	                			<?php foreach ($lists as $l) {?>
	                			<option value="<?php echo $l->id; ?>" <?php if ($l->id == set_value('produkn')) {echo "selected";}?> ><?php echo $l->produk; ?></option>
	                			<?php }?>
	                			<?php } else {?>
	                			<option value="">Tiada Produk</option>
	                			<?php }?>
	                		</select>
	                	</form>
	                </div>
	                <div class="box-body table-responsive no-padding">
	                	<table class="table table-hover">
	                		<tr class="active">
	                			<th class="text-center"> # </th>
	                			<th class="text-center"> Date </th>
	                			<th width="50%"> Product </th>
	                			<th class="text-center"> Stock Before </th>
	                			<th class="text-center"> Stock After </th>
	                			<th class="text-center"> Action </th>
	                		</tr>
	                		<?php if (is_array($loglist)) {
	?>
	                		<?php $i = 1;foreach ($loglist as $ll) {
		?>
							<?php
if ($ll->type == 1) {
			$t = "Add (" . (($ll->new) - ($ll->last)) . ")";
			$color = 'class="success"';
		} else if ($ll->type == 2) {
			$t = "Deduct (" . (($ll->last) - ($ll->new)) . ")";
			$color = 'class="danger"';
		} else if ($ll->type == 4) {
			$t = "Product have been sold";
			$color = 'class="warning"';
		} else {
			$t = "Nothing change.";
			$color = "";
		}
		?>
	                		<tr <?php echo $color; ?>>
	                			<td class="text-center"><?php echo $i; ?> </td>
	                			<td class="text-center"><?php echo date('Y-m-d', $ll->time); ?></td>
	                			<td><?php echo strtoupper($ll->produk); ?></td>
	                			<td class="text-center"><?php echo $ll->last; ?></td>
	                			<td class="text-center"><?php echo $ll->new; ?></td>
	                			<td class="text-center"><?php echo $t; ?></td>
	                		</tr>
	                		<?php $i++;}?>
	                		<?php } else {?>
	                		<tr>
	                			<td class="text-center" colspan="6">Sorry. No product found!</td>
	                		</tr>
	                		<?php }?>
	                	</table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="row">
	    	<div class="col-xs-12">
	    		<h1 class="text-center"><a href="<?php echo site_url('dashboard'); ?>/products"><i class="fa fa-arrow-circle-left"></i> Back</a></h1>
	    	</div>
	    </div>
	</section>
</div>

<?php $this->load->view('vfooter');?>