<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fa fa-fw fa-edit"></i> Edit Order</h1>
		<ol class="breadcrumb">
			<li><a href="/dashboard"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Edit Order</li>
		</ol>
	</section>
	<!-- edit order -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
	   		<div class="col-md-12">
	      		<div class="box">
	        		<div class="box-header with-border">
		    			<h3 class="box-title">Edit Form</h3>
					</div>
					<div class="box-body">
		        		<form class="form-horizontal" action="<?php echo site_url('dashboard') . '/orders/editorder/' . $orders[0]->id; ?>" id="borang" name="borang" method="POST" ENCTYPE="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Customer Name</label>
									<div class="col-sm-10">
										<input type="text" name="name" data-required="1" class="form-control" value="<?php echo $orders[0]->name; ?>" placeholder="Eg: John Doe">
	        								<?php echo form_error('name', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Phone</label>
									<div class="col-sm-10">
										<input type="tel" name="phone" class="form-control" value="<?php echo $orders[0]->phone; ?>" placeholder="Eg: 0123456789">
	        								<?php echo form_error('phone', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input type="email" name="email" class="form-control" value="<?php echo $orders[0]->email; ?>" placeholder="Eg: admin@gmail.com">
	        								<?php echo form_error('email', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Address</label>
									<div class="col-sm-10">
										<input type="text" name="add1" class="form-control" placeholder="Address 1" value="<?php echo $orders[0]->add1; ?>">
	        								<?php echo form_error('add1', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"></label>
									<div class="col-sm-10">
										<input type="text" name="add2" class="form-control" placeholder="Address 2" value="<?php echo $orders[0]->add2; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Postcode</label>
									<div class="col-sm-10">
										<input type="text" name="postcode" class="form-control" placeholder="Postcode" value="<?php echo $orders[0]->postcode; ?>">
									<?php echo form_error('postcode', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Town</label>
									<div class="col-sm-10">
										<input type="text" name="cities" class="form-control" placeholder="Town" value="<?php echo $orders[0]->city; ?>">
									<?php echo form_error('cities', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">State</label>
									<div class="col-sm-10">
										<select class="bs-select form-control" name="states">
											<?php if (is_array($states)) {?>
											<option value="" selected disabled>Negeri</option>
											<?php foreach ($states as $s) {?>
											<option value="<?php echo $s->state_id; ?>" <?php if ($s->state_id == $orders[0]->states_id) {echo "selected";}?> ><?php echo $s->name; ?></option>
										<?php }}?>
										</select>
									<?php echo form_error('states', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Product & Quantity</label>
									<div class="col-sm-6">
										<select name="product" class="bs-select form-control">
										<?php if (is_array($products)) {?>
											<option value="" selected disabled>Please Choose</option>
										<?php foreach ($products as $p) {?>
											<option value="<?php echo $p->id; ?>" <?php if ($p->id == $orders[0]->pid) {echo 'selected';}?>><?php echo $p->produk; ?> (<?php echo $p->unit; ?> unit)</option>
										<?php }?>
										<?php } else {?>
											<option value="0">No product</option>
										<?php }?>
										</select>
	        								<?php echo form_error('product', '<p class="help-inline">', '</p>'); ?>
									</div>
									<div class="col-sm-4">
										<input type="number" name="unit" value="<?php echo $orders[0]->unit; ?>" class="form-control" />
	        								<?php echo form_error('unit', '<p class="help-inline">', '</p>'); ?>
									</div>
								</div>
			              		<div class="form-group">
			              			<label class="col-sm-2 control-label">Price (RM)</label>
			              			<div class="col-sm-5">
			              				<input type="number" name="price" class="form-control" value="<?php echo $orders[0]->price ?>" placeholder="Paid Amount" step=".01">
	        								<?php echo form_error('price', '<p class="help-inline">', '</p>'); ?>
			              			</div>
			              		</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Payment Method</label>
									<div class="col-sm-10">
										<select name="bank" class="bs-select form-control">
											<?php if (is_array($banks)) {?>
			              					<option value="" selected disabled>Please Choose</option>
			              					<?php foreach ($banks as $b) {?>
			              						<option value="<?php echo $b->id ?>" <?php if ($b->id == $orders[0]->bank) {echo "selected";}?> ><?php echo $b->bankname; ?></option>
			              					<?php }?>
			              					<?php }?>
			              				</select>
	        								<?php echo form_error('bank', '<p class="help-inline">', '</p>'); ?>
			              			</div>
			              		</div>
			              		<div class="form-group">
			              			<label class="col-sm-2 control-label">Date & Time</label>
			              			<div class="col-sm-5">
			              				<div class="input-group date" id="dp1">
										    <input type="text" class="form-control" name="date" placeholder="Date" value="<?php echo date('M/d/Y', strtotime($orders[0]->bankdate)); ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
										</div>
			              			</div>
			              			<div class="col-sm-5">
			              				<div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
			              					<input type="text" name="times" class="form-control" value="<?php echo $orders[0]->banktime; ?>" placeholder="Time">
			              					<span class="input-group-addon">
			              						<span class="glyphicon glyphicon-time"></span>
			              					</span>
	        								<?php echo form_error('times', '<p class="help-inline">', '</p>'); ?>
			              				</div>
			              			</div>
			              		</div>
			              		<div class="form-group">
			              			<label class="col-sm-2 control-label">Additional Note</label>
			              			<div class="col-sm-10">
			              				<textarea type="text" maxlength="200" name="notes" class="form-control" placeholder="Eg: Blue colour"><?php echo $orders[0]->info; ?></textarea>
			              			</div>
			              		</div>
			              		<div class="form-group">
			              			<label class="col-sm-2 control-label">Payment Receipt <br>(.jpg, .gif, .png & .pdf)<br><small> Max 5MB</small></label>
			              			<div class="col-sm-10">
			              				<?php $resit = $orders[0]->resit;?>
			              				<a href="<?php echo base_url() . 'uploads/resit/' . $resit; ?>" target="_blank">Previous receipt.</a>
			              				<input type="file" name="receipt" accept="image/gif, image/jpeg, application/pdf" value="<?php echo set_value('receipt'); ?>">
	        								<?php echo form_error('receipt', '<p class="help-inline">', '</p>'); ?>
			              			</div>
			              		</div>
			              		<div class="box-footer">
			              			<div class="col-sm-offset-2">
			              				<button type="submit" form="borang" class="btn btn-info" name="addorder">Update</button>
			              			</div>
			              		</div>
			              	</div>
			            </form>
		            </div>
	          	</div>
	      	</div>
	  	</div>
	  	<div class="row">
	  		<div class="col-xs-12">
	  			<1 class="text-center"><a href="<?php echo site_url('dashboard') . '/orders/vieworder/' . $orders[0]->id; ?>"><i class="fa fa-arrow-circle-left"></i> Back</a></h1>
	  		</div>
	  	</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>