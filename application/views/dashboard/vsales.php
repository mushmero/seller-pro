<?php $this->load->view('vheader');?>
<?php $this->load->view('vsidebar');?>

<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fas fa-cart-arrow-down"></i> Sales</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard'); ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a></li>
			<li class="active">Sales</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
    			<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
				<?php echo (isset($error)) ? "<div class=\"alert alert-error\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-aqua">
	            <div class="inner">
	              <h3>RM<?php if (is_array($today)) {echo sprintf('%0.2f', $today[0]->daily);} else {echo sprintf('%0.2f', 0);}?></h3>

	              <p>Today</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-bag"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
			</div>
			<div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-green">
	            <div class="inner">
	              <h3>RM<?php if (is_array($month)) {echo sprintf('%0.2f', $month[0]->monthly);} else {echo sprintf('%0.2f', 0);}?></h3>

	              <p>This Month</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-stats-bars"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
			</div>
			<div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-yellow">
	            <div class="inner">
	              <h3>RM<?php if (!empty($year)) {echo sprintf('%0.2f', $year[0]->yearly);} else {echo sprintf('%0.2f', 0);}?></h3>

	              <p>Yearly Sales</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-person-add"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
			</div>
			<div class="col-lg-3 col-xs-6">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	              <h3>RM<?php if (!empty($total)) {echo sprintf('%0.2f', $total[0]->total);} else {echo sprintf('%0.2f', 0);}?></h3>

	              <p>Total Sales</p>
	            </div>
	            <div class="icon">
	              <i class="ion ion-pie-graph"></i>
	            </div>
	            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
	          </div>
			</div>
		</div>
	</section>
</div>

<?php $this->load->view('vfooter');?>