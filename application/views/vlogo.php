<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(1);?>

<header class="header main-header" >
<?php if ($uri == "" || $uri == "login" || $uri == "forgot" || $uri == "reset" || $uri == "logout") {?>
      <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo WEBLOGO; ?>" width="30" height="30"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo WEBNAME; ?></b> <img src="<?php echo WEBLOGO; ?>" width="30" height="30"></span>
        </a>
          <nav class="navbar navbar-static-top" role="navigation"></nav>
<?php } else {
	?>
<?php $ci = &get_instance();
	$ci->load->library('session');
	$ci->load->model('musers');
	$data = $ci->session->userdata('user');
	$user = $ci->musers->getUserInfo($data['id']);?>
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo WEBLOGO; ?>" width="30" height="30"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?php echo $user['name']; ?></b> <img src="<?php echo WEBLOGO; ?>" width="30" height="30"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
          <!-- Sidebar toggle button-->
          <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
              <span class="fas fa-align-justify fa-white"></span>
            </a>
        </nav>
<?php }?>
</header>