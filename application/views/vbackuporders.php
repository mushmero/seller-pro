<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report Orders</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Report Orders">
    <meta name="author" content="mushmero">

    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="icon" type="image/x-icon" href="<?php echo WEBLOGO; ?>">
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box">
    				<div class="box-header with-border">
    					<div class="text-center">
    						<h2>Report Orders <br>
    						<strong><?php echo date('d M Y', strtotime($startdate)) . ' - ' . date('d M Y', strtotime($enddate)); ?></strong></h2>
    					</div>
    				</div>
    				<div class="box-body no-padding">
    					<table class="table table-bordered table-condensed" width="3508">
    						<tr>
    							<th class="text-center">#</th>
    							<th class="col-xs-3">Customer Details</th>
    							<th class="col-xs-3">Product Details</th>
    							<th class="col-xs-2">Payment Details</th>
    							<th class="col-xs-2">Postage Details</th>
    							<th class="col-xs-2 text-center">Status</th>
    						</tr>
    						<?php if (is_array($result)) {
	?>
    						<?php $i = 1;foreach ($result as $r) {
		?>
    						<tr>
    							<td class="text-center"><?php echo $i; ?></td>
    							<td align="justify">
    								Name: <strong><?php echo $r->name; ?></strong><br>
    								Address: <?php echo $r->add1 . ' ' . $r->add2 . ' ' . $r->postcode . ' ' . $r->city . ' ' . $r->state_name; ?><br>
    								Phone: <?php echo $r->phone; ?><br>
    								Email: <?php echo $r->email; ?>
    							</td>
    							<td>
    								Product: <?php echo $r->pname; ?><br>
    								Unit Price: <?php echo sprintf('%0.2f', (($r->price - $r->postprice) / $r->unit)) . ' (' . $r->unit . ')'; ?><br>
    								Add. Info: <?php if ($r->info != '') {
			echo $r->info;
		} else {
			echo 'N/A';
		}?>
    							</td>
    							<td>
    								Bank: <?php echo $r->shortname; ?><br>
    								Date: <?php echo date('d/m/Y', strtotime($r->bankdate)); ?><br>
    								Time: <?php echo $r->banktime; ?><br>
    								Amount Paid: RM <?php echo sprintf('%0.2f', $r->price); ?>
    							</td>
    							<td>
    								Tracking no: <?php echo $r->trackingno; ?>
    								Method: <?php if ($r->postmethod == 2) {
			echo 'Cash On Delivery';
		} else if ($r->postmethod == 1) {
			echo 'Postage';
		} else {
			echo 'N/A';
		}?><br>
    								Postage Cost: RM <?php echo sprintf('%0.2f', $r->postprice); ?>
    							</td>
    							<td>
    								<?php if ($r->iscancel == 1) {echo 'Canceled';}?>
		            				<?php if ($r->status == 0 && $r->iscancel != 1) {echo 'Fresh Order';}?>
		            				<?php if ($r->status == 1 && $r->iscancel != 1) {echo 'In process';}?>
		            				<?php if ($r->status == 2 && $r->iscancel != 1) {echo 'In Shipping';}?>
		            				<?php if ($r->status == 3 && $r->iscancel != 1) {echo 'Completed';}?>
    							</td>
    						</tr>
    						<?php $i++;}?>
    						<?php } else {?>
    							<tr>
    								<td colspan="6">
    									<div class="text-center">
    										<h3><strong>Sorry! No information found. Please try again.</strong></h3>
    									</div>
    								</td>
    							</tr>
    						<?php }?>
    					</table>
    				</div>
    				<div class="box-footer"></div>
    			</div>
    		</div>
    	</div>
    </div>
</body>
</html>