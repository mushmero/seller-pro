<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once "vendor/autoload.php";
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class Excelgenerator {
	public function generateOrders($creator, $title, $sheetname, $date, $result, $filename) {

		$ss = new Spreadsheet();
		$ss->getProperties()
			->setCreator($creator)
			->setLastModifiedBy($creator)
			->setTitle($title)
			->setSubject('Total Orders ' . date('dmY') . '-' . date('hiA'))
			->setDescription('Total Orders')
			->setKeywords('Report Data')
			->setCategory('Report');
		$ss->getActiveSheet()->setTitle($sheetname);
		$ss->createSheet();
		$ss->getActiveSheet()
			->setCellValue('A1', $title)
			->setCellValue('B1', $date)
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Customer Details')
			->setCellValue('C3', 'Product Details')
			->setCellValue('D3', 'Payment Details')
			->setCellValue('E3', 'Postage Details')
			->setCellValue('F3', 'Status');

		$s1 = array(
			'font' => [
				'bold' => true,
				'size' => 20,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
		);
		$s2 = array(
			'font' => [
				'bold' => true,
				'size' => 15,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				],
			],
			'fill' => [
				'fillType' => Fill::FILL_SOLID,
				'color' => ['argb' => 'ff9900'],
			],
		);
		$s3 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_TOP,
				'horizontal' => Alignment::HORIZONTAL_JUSTIFY,
				'wrapText' => true,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '808080'],
				],
			],
		);
		$s4 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '808080'],
				],
			],
		);
		$s5 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'font' => [
				'bold' => true,
				'size' => 14,
			],
		);

		if (!empty($result)) {
			$row = 4;
			$counter = 1;
			for ($i = 0; $i < count($result); $i++) {
				$arrData = array(
					'customer' => 'Name: ' . $result[$i]->name . "\nAddress: " . $result[$i]->add1 . ' ' . $result[$i]->add2 . ' ' . $result[$i]->postcode . ' ' . $result[$i]->city . ' ' . $result[$i]->state_name . "\nPhone: " . $result[$i]->phone . "\nEmail: " . $result[$i]->email,
					'product' => 'Product: ' . $result[$i]->pname . "\nUnit Price: " . sprintf('%0.2f', ($result[$i]->price - $result[$i]->postprice) / $result[$i]->unit) . ' (' . $result[$i]->unit . " unit) \nAdd. Info: " . (!empty($result[$i]->info == true) ? $result[$i]->info : 'N/A'),
					'payment' => 'Bank: ' . $result[$i]->shortname . "\nDate: " . date('d/m/Y', strtotime($result[$i]->bankdate)) . "\nTime: " . $result[$i]->banktime . "\nAmount Paid: RM" . sprintf('%0.2f', $result[$i]->price),
					'postage' => 'Tracking No: ' . (!empty($result[$i]->trackingno) == true ? $result[$i]->trackingno : 'N/A') . "\nMethod: " . ($result[$i]->postmethod == 1 ? 'COD' : 'Postage') . "\nPostage Cost: RM" . sprintf('%0.2f', $result[$i]->postprice),
					'status' => ($result[$i]->iscancel == 1 ? 'Canceled' : '') . ($result[$i]->status == 0 && $result[$i]->iscancel != 1 ? 'Fresh Order' : '') . ($result[$i]->status == 1 && $result[$i]->iscancel != 1 ? 'In Process' : '') . ($result[$i]->status == 2 && $result[$i]->iscancel != 1 ? 'In Shipping' : '') . ($result[$i]->status == 3 && $result[$i]->iscancel != 1 ? 'Completed' : ''),
				);
				$ss->getActiveSheet()->setCellValue('A' . $row, $counter);
				$ss->getActiveSheet()->fromArray($arrData, NULL, 'B' . $row);
				$ss->getActiveSheet()->getStyle('B' . $row . ':F' . $row)->applyFromArray($s3);
				$ss->getActiveSheet()->getStyle('A' . $row)->applyFromArray($s4);
				$row++;
				$counter++;
			}
		} else {
			$ss->getActiveSheet()->setCellValue('A4', 'Opps! No information found');
			$ss->getActiveSheet()->getStyle('A4:F4')->applyFromArray($s5);
			$ss->getActiveSheet()->mergeCells('A4:F4');
		}
		$ss->getActiveSheet()->mergeCells('A1:F1');
		$ss->getActiveSheet()->freezePane('A4');
		$ss->getActiveSheet()
			->getStyle('A1:F1')->applyFromArray($s1);
		$ss->getActiveSheet()
			->getStyle('A3:F3')->applyFromArray($s2);
		$ss->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$ss->getActiveSheet()->getColumnDimension('B')->setWidth(33);
		$ss->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$ss->getActiveSheet()->getColumnDimension('D')->setWidth(35);
		$ss->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$ss->getActiveSheet()->getColumnDimension('F')->setWidth(20);

		$writer = IOFactory::createWriter($ss, "Xlsx");
		if (!is_dir('uploads/excel/')) {
			mkdir('uploads/excel/', 0777, true);
		}
		//header('Content-Type: application/vnd.ms-excel');
		/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename);
		header('Cache-Control: max-age=0');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');*/
		$writer->save('uploads/excel/' . $filename);
		/*$writer->save('php://output');*/
		$ss->disconnectWorksheets();
		unset($ss);
		return true;
	}
	public function generateProduct($creator, $title, $sheetname, $date, $result, $filename) {
		$ss = new Spreadsheet();
		$ss->getProperties()
			->setCreator($creator)
			->setLastModifiedBy($creator)
			->setTitle($title)
			->setSubject('Total Orders ' . date('dmY') . '-' . date('hiA'))
			->setDescription('Total Orders')
			->setKeywords('Report Data')
			->setCategory('Report');
		$ss->getActiveSheet()->setTitle($sheetname);
		$ss->createSheet();
		$ss->getActiveSheet()
			->setCellValue('A1', $title)
			->setCellValue('B1', $date)
			->setCellValue('A3', '#')
			->setCellValue('B3', 'Product Name')
			->setCellValue('C3', 'Unit')
			->setCellValue('D3', 'Cost Price');

		$s1 = array(
			'font' => [
				'bold' => true,
				'size' => 20,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
		);
		$s2 = array(
			'font' => [
				'bold' => true,
				'size' => 15,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				],
			],
			'fill' => [
				'fillType' => Fill::FILL_SOLID,
				'color' => ['argb' => '0099ff'],
			],
		);
		$s3 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_TOP,
				'horizontal' => Alignment::HORIZONTAL_JUSTIFY,
				'wrapText' => true,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '808080'],
				],
			],
		);
		$s4 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_RIGHT,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '808080'],
				],
			],
		);
		$s5 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'font' => [
				'bold' => true,
				'size' => 14,
			],
		);

		if (!empty($result)) {
			$row = 4;
			$counter = 1;
			for ($i = 0; $i < count($result); $i++) {
				$arrData = array(
					'name' => $result[$i]->produk,
					'unit' => $result[$i]->unit,
					'cost' => 'RM' . sprintf('%0.2f', $result[$i]->hargakos),
				);
				$ss->getActiveSheet()->setCellValue('A' . $row, $counter);
				$ss->getActiveSheet()->fromArray($arrData, NULL, 'B' . $row);
				$ss->getActiveSheet()->getStyle('B' . $row . ':D' . $row)->applyFromArray($s3);
				$ss->getActiveSheet()->getStyle('A' . $row)->applyFromArray($s4);
				$row++;
				$counter++;
			}
		} else {
			$ss->getActiveSheet()->setCellValue('A4', 'Opps! No information found');
			$ss->getActiveSheet()->getStyle('A4:D4')->applyFromArray($s5);
			$ss->getActiveSheet()->mergeCells('A4:D4');
		}
		$ss->getActiveSheet()->mergeCells('A1:D1');
		$ss->getActiveSheet()->freezePane('A4');
		$ss->getActiveSheet()
			->getStyle('A1:D1')->applyFromArray($s1);
		$ss->getActiveSheet()
			->getStyle('A3:D3')->applyFromArray($s2);
		$ss->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$ss->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$ss->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$ss->getActiveSheet()->getColumnDimension('D')->setWidth(35);
		$writer = IOFactory::createWriter($ss, "Xlsx");
		if (!is_dir('uploads/excel/')) {
			mkdir('uploads/excel/', 0777, true);
		}

		//header('Content-Type: application/vnd.ms-excel');
		/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename);
		header('Cache-Control: max-age=0');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');*/
		$writer->save('uploads/excel/' . $filename);
		/*$writer->save('php://output');*/
		$ss->disconnectWorksheets();
		unset($ss);
		return true;
	}
	public function generateSales($creator, $title, $sheetname, $date, $result, $filename) {
		$ss = new Spreadsheet();
		$ss->getProperties()
			->setCreator($creator)
			->setLastModifiedBy($creator)
			->setTitle($title)
			->setSubject('Total Sales ' . date('dmY') . '-' . date('hiA'))
			->setDescription('Total Sales')
			->setKeywords('Report Data')
			->setCategory('Report');
		$ss->getActiveSheet()->setTitle($sheetname);
		$ss->createSheet();
		$ss->getActiveSheet()
			->setCellValue('A1', $title)
			->setCellValue('B1', $date)
			->setCellValue('A3', 'Product Cost')
			->setCellValue('B3', 'Postage Cost')
			->setCellValue('C3', 'Ads Cost')
			->setCellValue('D3', 'Total Cost')
			->setCellValue('E3', 'Total Sales')
			->setCellValue('F3', 'Nett Profit');

		$s1 = array(
			'font' => [
				'bold' => true,
				'size' => 20,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
		);
		$s2 = array(
			'font' => [
				'bold' => true,
				'size' => 15,
			],
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '000000'],
				],
			],
			'fill' => [
				'fillType' => Fill::FILL_SOLID,
				'color' => ['argb' => 'ff7fc6'],
			],
		);
		$s3 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_TOP,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
				'wrapText' => true,
			],
			'borders' => [
				'allBorders' => [
					'borderStyle' => Border::BORDER_THIN,
					'color' => ['argb' => '808080'],
				],
			],
		);
		$s5 = array(
			'alignment' => [
				'vertical' => Alignment::VERTICAL_CENTER,
				'horizontal' => Alignment::HORIZONTAL_CENTER_CONTINUOUS,
			],
			'font' => [
				'bold' => true,
				'size' => 14,
			],
		);

		if (!empty($result)) {
			$arrData = array(
				'product' => 'RM' . sprintf('%0.2f', $result[0]->productcost),
				'postage' => 'RM' . sprintf('%0.2f', $result[0]->postagecost),
				'ads' => 'RM' . sprintf('%0.2f', $result[0]->adscost),
				'totalcost' => 'RM' . sprintf('%0.2f', ($result[0]->productcost + $result[0]->postagecost + $result[0]->adscost)),
				'totalsales' => 'RM' . sprintf('%0.2f', $result[0]->totalsales),
				'profit' => 'RM' . sprintf('%0.2f', ($result[0]->totalsales - ($result[0]->productcost + $result[0]->postagecost + $result[0]->adscost))),
			);
			$ss->getActiveSheet()->fromArray($arrData, NULL, 'A4');
			$ss->getActiveSheet()->getStyle('A4')->applyFromArray($s3);
		} else {
			$ss->getActiveSheet()->setCellValue('A4', 'Opps! No information found');
			$ss->getActiveSheet()->getStyle('A4:F4')->applyFromArray($s5);
			$ss->getActiveSheet()->mergeCells('A4:F4');
		}

		$ss->getActiveSheet()->mergeCells('A1:F1');
		$ss->getActiveSheet()
			->getStyle('A1:F1')->applyFromArray($s1);
		$ss->getActiveSheet()
			->getStyle('A3:F3')->applyFromArray($s2);
		$ss->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$ss->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$ss->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$ss->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$ss->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$ss->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$writer = IOFactory::createWriter($ss, "Xlsx");
		if (!is_dir('uploads/excel/')) {
			mkdir('uploads/excel/', 0777, true);
		}

		//header('Content-Type: application/vnd.ms-excel');
		/*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename);
		header('Cache-Control: max-age=0');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');*/
		$writer->save('uploads/excel/' . $filename);
		/*$writer->save('php://output');*/
		$ss->disconnectWorksheets();
		unset($ss);
		return true;
	}
}