<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once "vendor/autoload.php";
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

class Pdfgenerator {
	public function generateInvoice($html, $filename, $paper, $orientation, $stream = TRUE) {
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		$dompdf->setPaper($paper, $orientation);
		$dompdf->render();
		if ($stream) {
			$dompdf->stream($filename, array("attachment" => false));
		} else {
			$output = $dompdf->output();
			if (!is_dir('uploads/invoice/')) {
				mkdir('uploads/invoice/', 0777, true);
			}
			return file_put_contents('uploads/invoice/' . $filename, $output);
		}
	}
	public function generateBackup($html, $filename, $paper, $orientation, $stream = TRUE) {
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		$dompdf->setPaper($paper, $orientation);
		$dompdf->render();
		if ($stream) {
			$dompdf->stream($filename, array("attachment" => false));
		} else {
			$output = $dompdf->output();
			if (!is_dir('uploads/backup/')) {
				mkdir('uploads/backup/', 0777, true);
			}
			return file_put_contents('uploads/backup/' . $filename, $output);
		}
	}
}