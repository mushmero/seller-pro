<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once "vendor/autoload.php";
use PHPMailer;

class Mailer {
	public function reset_pass_mailer($data) {
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPAuth = true;
		$mail->Host = $data['host'];
		$mail->Port = $data['port'];
		$mail->Username = $data['username'];
		$mail->Password = $data['password'];
		$mail->SMTPSecure = $data['secure'];
		$mail->From = $data['from'];
		$mail->FromName = $data['name'];
		$mail->addAddress($data['to']);
		$mail->Subject = $data['subject'];
		$mail->Body = $data['content'];
		$mail->isHTML(true);

		return $mail->send();
	}
}