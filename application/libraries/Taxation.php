<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Taxation {
	public function calc_gst($rate, $item, $post) {
		$percent = $rate / 100;
		return ($item + $post) * $percent;
	}
}