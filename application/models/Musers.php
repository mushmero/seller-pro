<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends CI_Model {
	var $uTable = 'users';
	var $tokenTable = 'tokens';
	var $levelTable = 'level';
	var $sTable = 'states';
	var $maxIdle = 3600;

	function __construct() {
		parent::__construct();
		$this->load->library('systools');
	}
	private function generateSalt($length = 10) {
		$list = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXY";
		$i = 0;
		$salt = "";
		while ($i < $length) {
			$salt .= $list[mt_rand(0, strlen($list) - 1)];
			$i++;
		}
		return $salt;
	}
	function isLoggedIn() {
		$last_activity = $this->session->userdata('last_activity');
		$logged_in = $this->session->userdata('logged_in');
		$user = $this->session->userdata('user');
		$ip = $this->systools->getIP();

		if (($logged_in == 'yes') && ((time() - $last_activity) < $this->maxIdle)) {
			$this->allow_pass($user);
			return true;
		} else {
			$this->remove_pass();
			return false;
		}
	}
	function allow_pass($user_data) {
		$this->session->set_userdata(array('last_activity' => time(), 'logged_in' => 'yes', 'user' => $user_data));
	}
	function setLoginData($data) {
		$arr = array(
			'lastlogin' => time(),
			'ip' => $this->systools->getIp(),
		);
		$this->db->set($arr)->where('id', $data['id'])->update($this->uTable);
	}
	function remove_pass() {
		$this->session->unset_userdata('last_activity');
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user');
	}
	function checkUser($username) {
		$query = $this->db->get_where($this->uTable, array('username' => $username), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		}
		return false;
	}
	function saltedHash($password) {
		$salt = $this->generateSalt();
		return $salt . '.' . md5($salt . $password);
	}
	function verifyPassword($password, $hashed_password) {
		list($salt, $hash) = explode('.', $hashed_password);
		$hashed2 = $salt . '.' . md5($salt . $password);
		return ($hashed_password == $hashed2);
	}
	function save($user_data) {
		$this->db->insert($this->uTable, $user_data);
		return $this->db->insert_id();
	}
	function update($user_data) {
		if (isset($user_data['id'])) {
			$this->db->where('id', $user_data['id']);
			$this->db->update($this->uTable, $user_data);
			return $this->db->affected_rows();
		}
		return false;
	}
	function get_user_by_email($email) {
		$this->db->select('id');
		$this->db->from($this->uTable);
		$this->db->where('email', $email);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		} else {
			return false;
		}
	}
	function insertToken($id) {
		$token = substr(sha1(rand()), 0, 30);
		$date = date('Y/m/d');
		$string = array('token' => $token, 'user_id' => $id, 'date_created' => $date);
		$this->db->insert($this->tokenTable, $string);
		return $token . $id;
	}
	function checkToken($token) {
		$tkn = substr($token, 0, 30);
		$uid = substr($token, 30);

		$query = $this->db->get_where($this->tokenTable, array('token' => $tkn, 'user_id' => $uid), 1);

		if ($this->db->affected_rows() > 0) {
			$row = $query->row();
			$dateCreated = $row->date_created;
			$dateCreated1 = strtotime($dateCreated);
			$today = date('Y-m-d');
			$today1 = strtotime($today);
			if ($dateCreated1 != $today1) {
				return false;
			}
			$userInfo = $this->getUserInfo($row->user_id);
			return (object) $userInfo;
		}
		return false;
	}
	function getUserInfo($uid) {
		$raw = "select a.id, a.name, a.username, a.email, a.address, a.poskod, a.town, a.state, c.name as states_name, a.phone, a.level, b.levelName, a.status, a.lastlogin, a.ip, a.company, a.gst, a.gstrate, a.picture, a.smsname, a.smspass, a.smson from $this->uTable a left join $this->levelTable b on a.level = b.id left join $this->sTable c on a.state = c.state_id where a.id = $uid";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	function getUserId($id) {
		$query = $this->db->get_where($this->uTable, array('id' => $id), 1);
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		} else {
			return false;
		}
	}
	function updatePassword($data) {
		$query = $this->db->set('password', $data['password'])->where('id', $data['id'])->update($this->uTable);
		return $this->db->affected_rows();
	}
	function updateSMSDetails($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->uTable);
		return $this->db->affected_rows();
	}
	function updateUserDetails($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->uTable);
		return $this->db->affected_rows();
	}
}