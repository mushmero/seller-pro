<?php defined('BASEPATH') OR exit('No direct script access allowed');

class mDashboard extends CI_Model {

	var $pTable = 'produk';
	var $bTable = 'banks';
	var $sTable = 'states';
	var $oTable = 'orders';
	var $lTable = 'logstok';
	var $postage = 'postage';

	function __construct() {
		parent::__construct();
	}
	function getBankAll() {
		$data = file_get_contents('https://gist.githubusercontent.com/mushmero/b5d00d1bd874c0261956742552a39feb/raw/fc0732d2f5cf128eb18a82d933f9d4b48b59132e/json');
		if (isset($data)) {
			return $data;
		} else {
			return false;
		}
	}
	function getBank() {
		$query = $this->db->from($this->bTable)->order_by('bankname', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getStates() {
		$query = $this->db->from($this->sTable)->order_by('name', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function insOrder($data) {
		$query = $this->db->insert($this->oTable, $data);
		return $this->db->insert_id();
	}
	function insProduct($data) {
		$query = $this->db->insert($this->pTable, $data);
		return $this->db->insert_id();
	}
	function getProductAll() {
		$query = $this->db->from($this->pTable)->order_by('produk', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getProduct($u) {
		$query = $this->db->from($this->pTable)->where(array('status' => 1, 'user' => $u, 'unit >' => 0))->order_by('produk', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getProductByLimit($limit, $start) {
		$query = $this->db->limit($limit, $start)->order_by('produk', 'ASC')->get($this->pTable);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			return false;
		}
	}
	function countProduct() {
		$query = $this->db->count_all($this->pTable);
		return $query;
	}
	function removeProduct($id) {
		$this->db->delete($this->pTable, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	function getProductUnit($id) {
		$query = $this->db->where('id', $id)->get($this->pTable);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	function updateStock($data, $id) {
		$query = $this->db->set('unit', $data['unit'])->where('id', $id)->update($this->pTable);
		return $this->db->affected_rows();
	}
	function insertLogStock($data) {
		$query = $this->db->insert($this->lTable, $data);
		return $this->db->insert_id();
	}
	function getLogStockAll($id, $a) {
		$raw = "select a.last, a.new, a.time, a.date, a.type, b.produk from $this->lTable a left join $this->pTable b on a.produk = b.id where a.referrer = $id order by time desc limit 0, $a";
		/*$query = $this->db->limit($a, 0)->order_by('time', 'DESC')->where('referrer', $id)->get($this->lTable);*/
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getLogStockbyProduct($id, $pid, $a) {
		$raw = "select a.last, a.new, a.time, a.date, a.type, b.produk from $this->lTable a left join $this->pTable b on a.produk = b.id where a.referrer = $id and a.produk = $pid order by time desc limit 0, $a";
		/*$query = $this->db->limit($a, 0)->order_by('time', 'DESC')->where(array('referrer' => $id, 'produk' => $pid))->get($this->lTable);*/
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function countLogStockAll($id) {
		$query = $this->db->where('referrer', $id)->count_all($this->lTable);
		return $query;
	}
	function countLogStockbyProduct($id, $pid) {
		$query = $this->db->where(array('referrer' => $id, 'produk' => $pid))->count_all($this->lTable);
		return $query;
	}
	function getProductbyId($id) {
		$query = $this->db->where('id', $id)->get($this->pTable);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return $this->db->error();
		}
	}
	function updateProduct($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->pTable);
		return $this->db->affected_rows();
	}
	function countOrdersAll() {
		$query = $this->db->count_all($this->oTable);
		return $query;
	}
	function getOrdersbyTime($limit, $start) {
		$raw = "select a.id, a.name, a.add1, a.add2, a.postcode, a.city, a.states_id, e.name as states_name, a.phone, a.email, a.status, a.unit, a.referrer, a.trackingno, a.trackingdate, a.product as pid, b.produk, a.price, a.print, a.time, a.date, a.bank, c.bankname, a.bankdate, a.banktime, a.sellerid, a.pdflink, a.resit, a.postage, a.postmethod, a.update_time, a.update_date, a.update_user, d.area, d.price as postprice, a.iscancel, a.info from $this->oTable a left join $this->pTable b on a.product = b.id left join $this->bTable c on a.bank = c.id left join $this->postage d on a.postage = d.id left join $this->sTable e on a.states_id = e.state_id order by a.time desc limit $start, $limit";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			/*foreach ($query->result() as $row) {
				$data[] = $row;
			}*/
			return $query->result();
		} else {
			return false;
		}
	}
	function getOrdersbyId($id) {
		$raw = "select a.id, a.name, a.add1, a.add2, a.postcode, a.city, a.states_id, e.name as states_name, a.phone, a.email, a.status, a.unit, a.referrer, a.trackingno, a.trackingdate, a.product as pid, b.produk, a.price, a.print, a.time, a.date, a.bank, c.bankname, a.bankdate, a.banktime, a.sellerid, a.pdflink, a.resit, a.postage, a.postmethod, a.update_time, a.update_date, a.update_user, d.area, d.price as postprice, a.iscancel, a.info from $this->oTable a left join $this->pTable b on a.product = b.id left join $this->bTable c on a.bank = c.id left join $this->postage d on a.postage = d.id left join $this->sTable e on a.states_id = e.state_id where a.id = $id";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function countOrdersbyID($id, $user) {
		$raw = "select count(*) from $this->oTable where id = $id and status = 1 and (referrer = $user or sellerid = $user)";
		$query = $this->db->query($raw);
		return $query;
	}
	function updateOrderStatus($id) {
		$query = $this->db->set(array('status' => 1, 'print' => 1))->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function updatePostage($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function updateCanceledOrder($value, $id) {
		$query = $this->db->set('iscancel', $value)->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function updateCompleteOrder($value, $id) {
		$query = $this->db->set('status', $value)->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function updOrder($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function updateTracking($data, $id) {
		$query = $this->db->set($data)->where('id', $id)->update($this->oTable);
		return $this->db->affected_rows();
	}
	function backupOrders($start, $end) {
		$raw = "select a.name, a.add1, a.add2, a.postcode, a.city, c.name as state_name, a.phone, a.email, a.info, a.unit, a.product as pid, b.produk as pname, a.price, d.shortname, a.bankdate, a.banktime, a.pdflink, a.resit, a.postage, e.area, e.price as postprice, a.postmethod, a.trackingno, a.trackingdate, a.status, a.iscancel, a.referrer, a.print, a.time, a.date, a.update_time, a.update_date, a.update_user from $this->oTable a left join $this->pTable b on a.product = b.id left join $this->sTable c on a.states_id = c.id left join $this->bTable d on a.bank = d.id left join $this->postage e on a.postage = e.id where (a.date >= '$start' and a.date <= '$end' )";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function backupProduct($start, $end) {
		$raw = "select produk, unit, hargakos, date from $this->pTable where (date >= '$start' and date <= '$end')";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function backupSales($start, $end) {
		$raw = "select sum(a.price) as totalsales, sum(b.price) as postagecost, sum(c.hargakos) as productcost from $this->oTable a left join $this->postage b on a.postage = b.id left join $this->pTable c on a.product = c.id  where (a.date >= '$start' and a.date <= '$end')";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getTodaySales($date) {
		$query = $this->db->select('sum(price) as daily')->where(array('date' => $date, 'status' => 2))->get($this->oTable);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getMonthSales($value) {
		$raw = "select sum(price) as monthly from $this->oTable where (date_format(date,'%m') = '$value') and status = 2";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getYearlySales($value) {
		$raw = "select sum(price) as yearly from $this->oTable where (date_format(date,'%Y') = '$value') and status = 2";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getTotalSales() {
		$query = $this->db->select('sum(price) as total')->where('status', 2)->get($this->oTable);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getNewOrders($date) {
		$raw = "select count(id) as count from $this->oTable where (status = 0 or status = 1) and date = '$date'";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getMonthlyOrders($value) {
		$raw = "select count(id) as count from $this->oTable where (status = 0 or status = 1) and date_format(date,'%m')='$value'";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getTotalOrders() {
		$raw = "select count(id) as count from $this->oTable where (status = 1 or status = 0 or status = 2)";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getRejectOrders() {
		$raw = "select count(id) as count from $this->oTable where iscancel = 1";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getNettProfit() {
		$raw = "select sum(a.price) as totalsales, sum(b.price) as postagecost, sum(c.hargakos * a.unit) as productcost from $this->oTable a left join $this->postage b on a.postage = b.id left join $this->pTable c on a.product = c.id where a.status = 2";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getMonth() {
		$raw = "select date from orders group by date_format(date,'%m')";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function graphMonth($data) {
		$raw = "select price, date_format(date,'%d %b') as date from orders where date_format(date,'%m') = $data group by date";
		$query = $this->db->query($raw);
		return json_encode($query->result_array());
	}
	function graphAll() {
		$raw = "select price, date_format(date,'%b %Y') as date from orders group by date_format(date,'%m')";
		$query = $this->db->query($raw);
		return json_encode($query->result_array());
	}
	function countTotalProduct() {
		$query = $this->db->count_all($this->pTable);
		return $query;
	}
	function countAvailProduct() {
		$query = $this->db->where('status', 1)->count_all($this->pTable);
		return $query;
	}
	function getTotalUnit() {
		$raw = "select sum(unit) as total from $this->pTable where status = 1";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getUnitSold() {
		$raw = "select sum(unit) as sold from $this->oTable where iscancel = 0 and status = 2";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getAcceptedOrders() {
		$raw = "select count(id) as accept from $this->oTable where iscancel = 0";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	function getPostedOrders() {
		$raw = "select count(id) as posted from $this->oTable where (postage = 3 or (postage = 1 and trackingno != '') or (postage = 2 and trackingno !='')) and iscancel = 0; ";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	function getTotalCost() {
		$raw = "select sum(b.price) as postagecost, sum(c.hargakos * a.unit) as productcost from $this->oTable a left join $this->postage b on a.postage = b.id left join $this->pTable c on a.product = c.id where a.status = 2";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getExpectedSales() {
		$query = $this->db->select('sum(price) as expected')->get($this->oTable);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getCurrentSales() {
		$query = $this->db->select('sum(price) as current')->where(array('iscancel' => 0, 'status' => 2))->get($this->oTable);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
}